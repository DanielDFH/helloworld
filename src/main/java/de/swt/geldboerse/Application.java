package de.swt.geldboerse;

import de.swt.geldboerse.config.WebServerConfig;
import de.swt.geldboerse.config.DatabaseConfig;
import de.swt.geldboerse.controller.*;
import de.swt.geldboerse.util.Filters;
import de.swt.geldboerse.util.ModalMessageBuilder;
import de.swt.geldboerse.util.Path;
import de.swt.geldboerse.util.ViewUtil;
import org.cfg4j.provider.ConfigurationProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.Spark;

public class Application implements AutoCloseable {

    public static Logger LOGGER = LoggerFactory.getLogger(Application.class);

    private DatabaseConfig databaseConfig;
    private WebServerConfig webServerConfig;
    private Database database;

    public Application(ConfigurationProvider configurationProvider) {
        LOGGER.info("Starting application...");
        this.loadConfig(configurationProvider);
        this.setupDatabase();
        this.setupSpark();
    }

    private void loadConfig(ConfigurationProvider configurationProvider) {
        LOGGER.info("Loading config ...");
        try {
            databaseConfig = configurationProvider.bind("database", DatabaseConfig.class);
            webServerConfig = configurationProvider.bind("webserver", WebServerConfig.class);
        } catch (IllegalStateException e) {
            LOGGER.error("Unable to load config.");
            throw new RuntimeException("Config is missing or invalid.");
        }
    }

    private void setupDatabase() {
        database = new Database(databaseConfig);
    }

    private void setupSpark() {
        LOGGER.info("Starting web server ...");

        Spark.ipAddress(webServerConfig.host());
        Spark.port(webServerConfig.port());

        Spark.staticFileLocation("/public");

        Spark.after("*", Filters.logResponse);

        Spark.before("*", Filters.addTrailingSlashes);
        Spark.before("*", Filters.addLoginCheck); // Protect pages from anonymous users

        Spark.before(Path.Web.LOGIN, Filters.redirectIfLoggedIn); // Redirect to dashboard if already logged in
        Spark.before(Path.Web.REGISTER, Filters.redirectIfLoggedIn); // Redirect to dashboard if already logged in

        ModalMessageBuilder.setup(database);
        new LoginController(database).setup();
        new UserController(database).setup();
        new CurrencyController(database).setup();
        new PersonController(database).setup();
        new LocationController(database).setup();
        new TransactionController(database).setup();

        Spark.notFound(ViewUtil.notFound);
        Spark.internalServerError(ViewUtil.internalServerError);

        Spark.awaitInitialization(); // Block current thread till web server has initialized.
    }

    @Override
    public void close() {
        LOGGER.info("Stopping web server ...");
        Spark.stop();

        database.close();
    }

    public Database getDatabase() {
        return database;
    }
}
