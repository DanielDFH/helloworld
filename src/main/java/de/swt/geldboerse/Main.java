package de.swt.geldboerse;

import de.swt.geldboerse.config.ConfigProvider;

public class Main {
	public static void main(String[] args) {
	    final Application application = new Application(ConfigProvider.configurationProvider());
        Runtime.getRuntime().addShutdownHook(new Thread(application::close));
	}
}
