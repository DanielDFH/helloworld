package de.swt.geldboerse;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import de.swt.geldboerse.config.DatabaseConfig;
import de.swt.geldboerse.model.*;
import de.swt.geldboerse.util.DummyData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Database implements AutoCloseable {
    private static Logger LOGGER = LoggerFactory.getLogger(Database.class);

    private Dao<Currency, String> currencyDao;
    private Dao<Credit, Integer> creditDao;
    private Dao<User, String> userDao;
    private Dao<Location, String> locationDao;
    private Dao<Category, String> categoryDao;
    private Dao<Person, Integer> personDao;
    private Dao<Transaction, Integer> transactionDao;
    private ConnectionSource connectionSource;

	public Database(DatabaseConfig config) {
        LOGGER.info("Opening database connection ...");
		String databaseUrl = "jdbc:sqlite:"+config.name();
		try {
			connectionSource = new JdbcConnectionSource(databaseUrl);

			currencyDao    = DaoManager.createDao(connectionSource, Currency.class);
			creditDao      = DaoManager.createDao(connectionSource, Credit.class);
			userDao        = DaoManager.createDao(connectionSource, User.class);
			locationDao    = DaoManager.createDao(connectionSource, Location.class);
			categoryDao    = DaoManager.createDao(connectionSource, Category.class);
			personDao      = DaoManager.createDao(connectionSource, Person.class);
			transactionDao = DaoManager.createDao(connectionSource, Transaction.class);

			if(config.createTables())
			    createTables();

			if(config.setupDummyData())
                DummyData.setup(this);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
        try {
            LOGGER.info("Closing database connection ...");
            connectionSource.close();
        } catch (IOException e) {
            LOGGER.error("Unable to close database!");
            e.printStackTrace();
        }
    }

	public Dao<Currency, String> getCurrencyDao() {
		return currencyDao;
	}

	public Dao<User, String> getUserDao() {
		return userDao;
	}

	public Dao<Location, String> getLocationDao() {
		return locationDao;
	}

	public Dao<Category, String> getCategoryDao() {
		return categoryDao;
	}

	public Dao<Person, Integer> getPersonDao() {
		return personDao;
	}

	public Dao<Transaction, Integer> getTransactionDao() {
		return transactionDao;
	}

	private void createTables() {
		try {
			TableUtils.createTableIfNotExists(connectionSource, Currency.class);
			TableUtils.createTableIfNotExists(connectionSource, Credit.class);
			TableUtils.createTableIfNotExists(connectionSource, User.class);
			TableUtils.createTableIfNotExists(connectionSource, Location.class);
			TableUtils.createTableIfNotExists(connectionSource, Category.class);
			TableUtils.createTableIfNotExists(connectionSource, Person.class);
			TableUtils.createTableIfNotExists(connectionSource, Transaction.class);
		} catch (SQLException e) {
			throw new RuntimeException("Can't setup tables in database.", e);
		}
	}
}
