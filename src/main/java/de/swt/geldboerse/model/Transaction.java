package de.swt.geldboerse.model;
import java.util.Date;
import java.util.List;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "transaction")
public class Transaction {
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(canBeNull = false)
	private double amount;
	@DatabaseField(canBeNull = false)
	private Date date;
	@DatabaseField(canBeNull = false)
	private double euroAmount;
	@DatabaseField(canBeNull = false, foreign = true)
	private User user;
	@DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
	private Currency currency;
	@DatabaseField(foreign = true)
	private Category category;
	@Deprecated
	private List<Category> categoriesAsList;
	@DatabaseField(foreign = true)
	private Person person;
	@DatabaseField(foreign = true)
	private Location location;

	public Transaction() {

	}

	public Transaction(double amount, Date date, double euroAmount, User user, Currency currency, Category category, Location location) {
		this.amount = amount;
		this.date = date;
		this.euroAmount = euroAmount;
		this.user = user;
		this.currency = currency;
		this.category = category;
        this.location = location;
	}

	public Transaction(double amount, Date date, double euroAmount, User user, Currency currency, Category category, Person person) {
		this.amount = amount;
		this.date = date;
		this.euroAmount = euroAmount;
		this.user = user;
		this.currency = currency;
		this.category = category;
		this.person = person;
	}

	public Transaction(double amount, double euroAmount, Currency currency, List<Category> categoriesAsList, Location location) {
		this.amount = amount;
		this.date = new Date();
		this.euroAmount = euroAmount;
		this.currency = currency;
		this.categoriesAsList = categoriesAsList;
		this.location = location;
	}

	public Transaction(double amount, double euroAmount, Currency currency, List<Category> categoriesAsList, Person person) {
		this.amount = amount;
		this.date = new Date();
		this.euroAmount = euroAmount;
		this.currency = currency;
		this.categoriesAsList = categoriesAsList;
		this.person = person;
	}

	public int getId() {
		return id;
	}

	public double getAmount() {
		return amount;
	}

	public Date getDate() {
		return date;
	}

	public double getEuroAmount() {
		return euroAmount;
	}

	public User getUser() {
		return user;
	}

	public Currency getCurrency() {
		return currency;
	}

	public Category getCategory() {
		return category;
	}

	@Deprecated
	public List<Category> getCategoriesAsList() {
		return categoriesAsList;
	}

	public Person getPerson() {
		return person;
	}

	public Location getLocation() {
		return location;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setEuroAmount(double euroAmount) {
		this.euroAmount = euroAmount;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public boolean hasPerson(){
		return this.person != null;
	}
}
