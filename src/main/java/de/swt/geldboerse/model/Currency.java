package de.swt.geldboerse.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "currency")
public class Currency {

	@DatabaseField(id = true)
	private String name;
	@DatabaseField
	private double euroFactor;

	Transaction transaction;


	public Currency() {

	}

	public Currency(String name) {
		this.name = name;
	}

	public Currency(String name, double euroFactor) {
		this.name = name;
		this.euroFactor = euroFactor;
	}


	public double convert(double amount, Currency currency) {
		return fromEuro(currency.toEuro(amount));
	}

	public double toEuro(double amount){
		return amount / this.euroFactor;
	}

	public double fromEuro(double amount){
		return amount * this.euroFactor;
	}

    /**
     * Just an alias for #getName.
     */
	public String getId() {
	    return name;
    }

	public String getName() {
		return name;
	}

	public double getEuroFactor() {
		return euroFactor;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEuroFactor(double euroFactor) {
		this.euroFactor = euroFactor;
	}

	public String toString() {
		return name;
	}

}
