package de.swt.geldboerse.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "category")
public class Category {

	@DatabaseField(id = true)
	private String name;

	public Category() {

	}

	public Category(String name) {
		this.name = name;
	}

	public String getId() {
	    return name;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
