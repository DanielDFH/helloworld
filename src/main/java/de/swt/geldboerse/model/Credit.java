package de.swt.geldboerse.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "credit")
public class Credit {

	@DatabaseField(id = true)
	private int id;
	@DatabaseField(foreign = true)
	private User user;
	@DatabaseField(canBeNull = false)
	private double amount;

	public Credit() {

	}

	public Credit(User user, double amount) {
		this.user = user;
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public User getUser() {
    	return user;
    }

    public void setUser(User user) {
    	this.user = user;
    }

    public double getAmount() {
    	return amount;
    }

    public void setAmount(double amount) {
    	this.amount = amount;
    }



}
