package de.swt.geldboerse.model;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

@DatabaseTable(tableName = "location")
public class Location {

	@DatabaseField(id = true)
    private String name;
    @DatabaseField
    private String street;
    @DatabaseField
    private String postcode;
    @DatabaseField
    private String comment;

    public Location() {

    }

	public Location(String name) {
		this.name = name;
	}

	public Location(String name, String street, String postcode, String comment) {
		this.name = name;
		this.street = street;
		this.postcode = postcode;
		this.comment = comment;
	}

	public String getId() {
    	return name;
    }

	public String getName() {
		return name;
	}

	public String getStreet() {
		return street;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getComment() {
		return comment;
	}

	public void setName(String name) {
    	this.name = name;
    }

    public void setStreet(String street) {
    	this.street = street;
    }

    public void setPostcode(String postcode) {
    	this.postcode = postcode;
    }

    public void setComment(String comment) {
    	this.comment = comment;
    }

    @Override
	public String toString() {
        final List<String> list = new ArrayList<>(Arrays.asList(name, street, postcode, comment));
        list.removeIf(Objects::isNull);
        return String.join(" ", list);
	}
}
