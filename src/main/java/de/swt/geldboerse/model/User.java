package de.swt.geldboerse.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import de.swt.geldboerse.Database;

import javax.xml.bind.DatatypeConverter;

@DatabaseTable(tableName = "user")
public class User {
	@DatabaseField(id = true)
	private String name;
	@DatabaseField(canBeNull = false)
	private String passwordHash;
	@DatabaseField(canBeNull = false)
	private String email;
    @DatabaseField(canBeNull = false)
    private double amount;
    @ForeignCollectionField
    private Collection<Transaction> transactions;

    public User() {

	}

	public User(String name, String password, String email) {
		this.name = name;
		this.email = email;
		setPassword(password);
	}

	public User(String name, String password) {
		this.name = name;
		setPassword(password);
	}

	public String getId() {
		return name;
	}

	@Override
    public String toString() {
        return name;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public static String createPasswordHash(String name, String password) {
		return hash(name + password);
	}

	public void setPassword(String password) {
		this.passwordHash = hash(this.name + password);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return this.email;
	}

	public boolean login(Database db) throws SQLException {
		User user = db.getUserDao().queryForSameId(this);
		return this.equals(user);
	}

	public void signup(String name, String password, String email) throws Exception {
	    setName(name);
	    setEmail(email);
	    setPassword(password);
	}

    private static MessageDigest messageDigest;
	static {
	    try {
	        messageDigest = MessageDigest.getInstance("MD5");
        } catch(NoSuchAlgorithmException e) {
	        throw new RuntimeException(e);
        }
    }

	/**
	* Berechnet die Hashsumme einer Zeichenkette mittels des MD5 Verfahren.
	* Liefert den MD5 Hash Base64 encoded zurueck.
	* @param s String von dem die Hashsumme gebildet werden soll
	* @return base64(hash(s))
	*/
	private static String hash(String s) {
	    return DatatypeConverter.printBase64Binary(messageDigest.digest(s.getBytes()));
	}

	public void logout() {

	}

	public boolean equals(User x) {
		return this.name.equals(x.name) && this.passwordHash.equals(x.passwordHash);
	}

	// ---- Wallet ----

    public double setCurrency(Currency currency) {
        return currency.fromEuro(amount);
    }

    public boolean changeAmount(double newAmount) {
        double temp = amount + newAmount;
        if(temp<0) {
            return false;
        }
        else if(temp>=0) {
            amount = temp;
            return true;
        }
        return false;
    }

    public Collection<Transaction> getTransactions() {
        return transactions;
    }

    public List<Transaction> getTransactions(Date von , Date bis) {
        List<Transaction> ret = new LinkedList<Transaction>();
        for(Transaction t : transactions) {
            if(t.getDate().after(von) && t.getDate().before(bis)) {
                ret.add(t);
            }
        }
        return ret;
    }

    public List<Transaction> getLendingTransactions() {
        List<Transaction> ret = new LinkedList<Transaction>();
        for(Transaction t : transactions) {
            if(t.hasPerson()) {
                ret.add(t);
            }
        }
        return ret;
    }

    public List<Transaction> getLendingTransactions(Date from, Date to) {
        List<Transaction> ret = new LinkedList<Transaction>();
        for(Transaction t : transactions) {
            if(t.getDate().after(from) && t.getDate().before(to) && t.hasPerson()) {
                ret.add(t);
            }
        }
        return ret;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double newAmount) {
        this.amount = newAmount;
    }

    public boolean checkInput(String s) {
        if(s.matches("\\d+((,|\\.)?\\d+)?$")) {
            return true;
        }
        else return false;
    }

    public double toDouble(String s) {
        double ret = Double.parseDouble(s.replace(",", "."));
        return ret;
    }

    public Transaction createTransaktionOrt(double euroBetrag, Currency currency, List<Category> listKategorien, Location location) {
        Transaction trans = new Transaction(amount, euroBetrag, currency, listKategorien, location);
        transactions.add(trans);
        return trans;
    }

    public Transaction createTransactionPerson(double euroBetrag, Currency currency, List<Category> listKategorien, Person person) {
        Transaction trans = new Transaction(amount, euroBetrag, currency, listKategorien, person);
        transactions.add(trans);
        return trans;
    }
}
