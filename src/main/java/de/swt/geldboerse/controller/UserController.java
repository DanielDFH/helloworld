package de.swt.geldboerse.controller;

import com.j256.ormlite.dao.Dao;
import de.swt.geldboerse.Database;
import de.swt.geldboerse.model.User;
import de.swt.geldboerse.util.CrudResource;
import spark.Request;

import java.util.Map;

public class UserController extends CrudResourceController<User,String> {
    private Dao<User,String> dao;

    public UserController(Database database) {
        super(database);
        dao = database.getUserDao();
    }

    protected CrudResource getResourceDefinition() {
        return CrudResource.USER;
    }

    protected Dao<User,String> getEntityDao() {
        return dao;
    }

    protected String parseEntityId(String id) {
        return id;
    }

    protected User setupEntityFromParams(Request req, String id, User user) {
        if(user == null) {
            user = new User();
            user.setName(id);
        }

        user.setEmail(getQueryParam(req, "email"));
        user.setPassword(getQueryParam(req, "password"));

        return user;
    }

    protected void setupEditModel(Map<String,Object> model, User user) {
        if(user != null) {
            model.put("id", user.getName());
            model.put("email", user.getEmail());
            model.put("password", "");
        }
    }
}
