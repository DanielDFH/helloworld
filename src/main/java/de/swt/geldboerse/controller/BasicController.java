package de.swt.geldboerse.controller;

import com.j256.ormlite.dao.Dao;
import de.swt.geldboerse.Database;
import de.swt.geldboerse.model.User;
import de.swt.geldboerse.util.CrudResource;
import de.swt.geldboerse.util.ViewUtil;
import spark.Request;
import spark.Session;

import java.sql.SQLException;
import java.util.Map;

public abstract class BasicController {
    protected BasicController(Database database) {
    }

    public abstract void setup();

    protected User getSessionUser(Request req) {
        final Session session = req.session(false);
        if(session != null)
            return session.attribute("user");
        return null;
    }

    protected String render(Request req, Map<String, Object> model, String templatePath) {
        final User user = getSessionUser(req);
        if(user != null) {
            model.put("user", user);
        }
        model.put("resources", CrudResource.values());
        return ViewUtil.render(req, model, templatePath);
    }

    protected <T,ID> T queryForExistingId(Dao<T,ID> dao, ID id) throws SQLException {
        final T entity = dao.queryForId(id);
        if(entity == null) {
            throw new RuntimeException(String.format("No entity (%s) with ID %s.",
                dao.getClass().toString(),
                id.toString()));
        }
        return entity;
    }

    protected String getRouteParam(Request req, String name) {
        final String value = req.params(name);
        if(value == null) {
            throw new RuntimeException(String.format("Parameter %s not in route.", name));
        }
        return value;
    }

    protected String getQueryParam(Request req, String name) {
        final String value = req.queryParams(name);
        if(value == null) {
            throw new RuntimeException(String.format("Parameter %s not sent by request.", name));
        }
        return value;
    }
}
