package de.swt.geldboerse.controller;

import com.j256.ormlite.dao.Dao;
import de.swt.geldboerse.Database;
import de.swt.geldboerse.util.CrudResource;
import de.swt.geldboerse.util.ModalMessageBuilder;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public abstract class CrudResourceController<T,ID> extends BasicController {
    private static final String idParamName = "id";

    protected CrudResourceController(Database database) {
        super(database);
    }

    public void setup() {
        final CrudResource resource = getResourceDefinition();
        final Dao<T,ID> dao = getEntityDao();

        // Show entity-creation form:
        Spark.get(resource.newEntityFormUrl(), (Request req, Response res) -> {
            Map<String, Object> model = new HashMap<>();
            setupEditModel(model, null);
            model.put("formActionUrl", resource.url());
            model.put("title", "New "+resource.capitalizedSingular());
            return render(req, model, getTemplate("edit"));
        });

        // Show single entity:
        Spark.get(resource.entityUrl(":"+idParamName), (Request req, Response res) -> {
            final ID id = getEntityIdFromRoute(req);
            final T entity = dao.queryForId(id);
            Map<String, Object> model = new HashMap<>();
            setupEditModel(model, entity);
            model.put("formActionUrl", resource.entityUrl(id.toString()));
            model.put("title", resource.capitalizedSingular());
            model.put("lockedId", true);
            return render(req, model, getTemplate("edit"));
        });

        // Create new entity:
        Spark.post(resource.url(), (Request req, Response res) -> {
            final ID id = tryGetEntityIdFromQuery(req);
            if(id != null && dao.idExists(id)) {
                return new ModalMessageBuilder()
                    .title("'"+id+"' exists already")
                    .description("There is another "+resource.singular()+" which has the same name. "+
                                 "Either pick a different name or edit the existing "+resource.singular()+".")
                    .icon("exclamation triangle")
                    .targetUrl(resource.url())
                    .render(req);
            }

            final T entity = setupEntityFromParams(req, id, null);
            dao.create(entity);
            res.redirect(resource.url());
            return null;
        });

        // Update existing entity:
        Spark.post(resource.entityUrl(":"+idParamName), (Request req, Response res) -> {
            final ID id = getEntityIdFromRoute(req);
            final T entity = queryForExistingId(dao, id);
            setupEntityFromParams(req, id, entity);
            dao.update(entity);
            res.redirect(resource.url());
            return null;
        });

        // Delete entity:
        Route delete = (Request req, Response res) -> {
            final ID id = getEntityIdFromRoute(req);
            dao.deleteById(id);
            res.redirect(resource.url());
            return null;
        };
        Spark.delete(resource.entityUrl(":"+idParamName), delete);
        Spark.get(resource.entityUrl(":"+idParamName)+"/delete/", delete); // Just a helper as forms can't send DELETE requests.

        // Show entity list:
        Spark.get(resource.url(), (Request req, Response res) -> {
            Map<String, Object> model = new HashMap<>();
            setupListModel(model);
            return render(req, model, getTemplate("list"));
        });
    }

    protected abstract CrudResource getResourceDefinition();

    protected String getTemplate(String basename) {
        final CrudResource resource = getResourceDefinition();
        return "pages/" + resource.plural() + "/" + basename + ".mustache";
    }

    protected abstract Dao<T,ID> getEntityDao();

    protected abstract ID parseEntityId(String id);

    protected ID getEntityIdFromRoute(Request req) {
        return parseEntityId(getRouteParam(req, idParamName));
    }

    protected ID getEntityIdFromQuery(Request req) {
        return parseEntityId(getQueryParam(req, idParamName));
    }

    protected ID tryGetEntityIdFromQuery(Request req) {
        final String idString = req.queryParams(idParamName);
        if(idString != null) {
            return parseEntityId(idString);
        } else {
            return null;
        }
    }

    protected abstract T setupEntityFromParams(Request req, ID id, T entity);

    protected abstract void setupEditModel(Map<String, Object> model, T entity);

    protected void setupListModel(Map<String, Object> model) {
        final Dao<T,ID> dao = getEntityDao();
        try {
            model.put("entities", dao.queryForAll());
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
