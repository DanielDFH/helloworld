package de.swt.geldboerse.controller;

import com.j256.ormlite.dao.Dao;

import de.swt.geldboerse.Application;
import de.swt.geldboerse.model.User;
import de.swt.geldboerse.util.Validation;
import de.swt.geldboerse.Database;
import de.swt.geldboerse.util.ModalMessageBuilder;
import de.swt.geldboerse.util.Path;
import de.swt.geldboerse.util.ViewUtil;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.util.HashMap;
import java.util.Map;

public class LoginController extends BasicController {
    private Dao<User, String> dao;

    public LoginController(Database database) {
        super(database);
        dao = database.getUserDao();
    }

    public void setup() {
        Spark.get("/login/", (Request req, Response res) -> {
                Map<String, Object> model = new HashMap<>();
                model.put("title", "Login");
                return render(req, model, Path.Template.LOGIN);
        });

        Spark.get("/register/", (Request req, Response res) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("title", "Sign-Up");

            return render(req, model, Path.Template.REGISTER);
        });

        // Create new entity:
        Spark.post("/register/", (Request req, Response res) -> {
            final String name = req.queryParams("user");
            final String email = req.queryParams("email");
            final String password = req.queryParams("password");
            final String passwordcheck = req.queryParams("passwordcheck");
            User benutzer = dao.queryForId(name);
            if(benutzer == null && Validation.isValidId(name)&& Validation.isValidPassword(password)
            		&& Validation.isValidPassword(email)&& Validation.isValidPasswordCheck(password, passwordcheck)) {
            	final User user = new User(name, password, email);
            	dao.create(user);
            	return new ModalMessageBuilder()
                    .title("Success")
                    .description(String.format("Welcome %s, you can now login using your account credentials!", name))
                    .icon("check circle")
                    .targetUrl(Path.Web.LOGIN)
                    .render(req);
            }
            return new ModalMessageBuilder()
                    .title("SIGN-UP ERROR")
                    .description("Seems like any fields are not or incorrectly filled. \nPlease try again.")
                    .icon("user times")
                    .targetUrl(Path.Web.REGISTER)
                    .render(req);
        });


        Spark.post("/login/", (Request req, Response res) -> {
            Map<String, Object> model = new HashMap<>();
            String userId = req.queryParams("user");
            String password = req.queryParams("password");
            User loginUser = new User(userId, password);
            if (Validation.isValidId(userId) && Validation.isValidPassword(password)) {
                // Input fields are valid
                model.put("title", "Übersicht");
                User benutzer = dao.queryForId(userId);
                if (benutzer != null) {
                    // User exists
                    if (loginUser.equals(benutzer)) {
                        // Password is correct
                        req.session().attribute("user", benutzer);
                        res.redirect(Path.Web.DEFAULT);
                        return null;
                    }
                }
            }
            Application.LOGGER.warn("Invalid login!");
            // Invalid or missing login parameters
            model.put("title", "Login Error");
            model.put("error", true);
            model.put("errorMsg", "ERROR_INVALID_LOGIN");
            return new ModalMessageBuilder()
                    .title("LOGIN ERROR")
                    .description("Seems like you took the wrong username or password. \nPlease try again.")
                    .icon("user")
                    .targetUrl("/login/")
                    .render(req);
        });


        Spark.get("/logout/", (Request req, Response res) -> {
        	if (req.session().attribute("user") != null) {
                req.session().removeAttribute("user");
                Map<String, Object> model = new HashMap<>();
                model.put("title", "Login");
                res.redirect(Path.Web.LOGIN);
            	return null;
            }
            return new ModalMessageBuilder()
                    .title("LOGOUT ERROR")
                    .description("Seems like you are not logged in.")
                    .icon("user")
                    .targetUrl("/login/")
                    .render(req);
        });

        Spark.post("/login/", (Request req, Response res) -> {
            if (req.session().attribute("user") != null) {
                req.session().removeAttribute("user");
                Map<String, Object> model = new HashMap<>();
                model.put("title", "Login");
                return ViewUtil.render(req, model, Path.Template.LOGIN);
            }
            return new ModalMessageBuilder()
                    .title("LOGOUT ERROR")
                    .description("Seems like you are not logged in.")
                    .icon("user")
                    .targetUrl("/login/")
                    .render(req);
        });
    }
}