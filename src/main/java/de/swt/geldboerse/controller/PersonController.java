package de.swt.geldboerse.controller;

import java.util.Map;

import com.j256.ormlite.dao.Dao;

import de.swt.geldboerse.Database;
import de.swt.geldboerse.model.Person;
import de.swt.geldboerse.util.CrudResource;
import spark.Request;

public class PersonController extends CrudResourceController<Person,Integer> {
    private Dao<Person,Integer> dao;

    public PersonController(Database database) {
        super(database);
        dao = database.getPersonDao();
    }

    protected CrudResource getResourceDefinition() {
        return CrudResource.PERSON;
    }

    protected Dao<Person,Integer> getEntityDao() {
        return dao;
    }

    protected Integer parseEntityId(String id) {
        return Integer.parseInt(id);
    }

    protected Person setupEntityFromParams(Request req, Integer id, Person person) {
        if(person == null) {
            person = new Person();
        }

        person.setFirstName(getQueryParam(req, "firstName"));
        person.setLastName(getQueryParam(req, "lastName"));

        return person;
    }

    protected void setupEditModel(Map<String,Object> model, Person person) {
        if(person != null) {
            model.put("firstName", person.getFirstName());
            model.put("lastName", person.getLastName());
        }
    }
}
