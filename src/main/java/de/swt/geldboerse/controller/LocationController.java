package de.swt.geldboerse.controller;

import java.util.Map;

import com.j256.ormlite.dao.Dao;

import de.swt.geldboerse.Database;
import de.swt.geldboerse.model.Location;
import de.swt.geldboerse.util.CrudResource;
import spark.Request;

public class LocationController extends CrudResourceController<Location,String> {
    private Dao<Location,String> dao;

    public LocationController(Database database) {
        super(database);
        dao = database.getLocationDao();
    }

    protected CrudResource getResourceDefinition() {
        return CrudResource.LOCATION;
    }

    protected Dao<Location,String> getEntityDao() {
        return dao;
    }

    protected String parseEntityId(String id) {
        return id;
    }

    protected Location setupEntityFromParams(Request req, String id, Location location) {
        if(location == null) {
            location = new Location();
            location.setName(id);
        }

        location.setStreet(getQueryParam(req, "street"));
        location.setPostcode(getQueryParam(req, "postcode"));
        location.setComment(getQueryParam(req, "comment"));

        return location;
    }

    protected void setupEditModel(Map<String,Object> model, Location location) {
        if(location != null) {
            model.put("id", location.getName());
            model.put("street", location.getStreet());
            model.put("postcode", location.getPostcode());
            model.put("comment", location.getComment());
        }
    }
}
