package de.swt.geldboerse.controller;

import com.j256.ormlite.dao.Dao;
import de.swt.geldboerse.Database;
import de.swt.geldboerse.model.Currency;
import de.swt.geldboerse.util.CrudResource;
import spark.Request;
import java.util.Map;

public class CurrencyController extends CrudResourceController<Currency,String> {
    private Dao<Currency,String> dao;

    public CurrencyController(Database database) {
        super(database);
        dao = database.getCurrencyDao();
    }

    protected CrudResource getResourceDefinition() {
        return CrudResource.CURRENCY;
    }

    protected Dao<Currency,String> getEntityDao() {
        return dao;
    }

    protected String parseEntityId(String id) {
        return id;
    }

    protected Currency setupEntityFromParams(Request req, String id, Currency currency) {
        if(currency == null) {
            currency = new Currency();
            currency.setName(id);
        }

        final double euroFactor = Double.parseDouble(getQueryParam(req, "euroFactor"));
        currency.setEuroFactor(euroFactor);

        return currency;
    }

    protected void setupEditModel(Map<String,Object> model, Currency currency) {
        if(currency != null) {
            model.put("id", currency.getName());
            model.put("euroFactor", currency.getEuroFactor());
        }
    }
}
