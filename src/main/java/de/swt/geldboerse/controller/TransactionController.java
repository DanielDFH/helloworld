package de.swt.geldboerse.controller;

import com.j256.ormlite.dao.Dao;
import de.swt.geldboerse.Database;
import de.swt.geldboerse.model.*;
import de.swt.geldboerse.model.Currency;
import de.swt.geldboerse.util.CrudResource;
import spark.Request;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class TransactionController extends CrudResourceController<Transaction,Integer> {
    private Dao<Transaction, Integer> transactionDao;
    private Dao<Currency, String> currencyDao;
    private Dao<User, String> userDao;
    private Dao<Category, String> categoryDao;
    private Dao<Person, Integer> personDao;
    private Dao<Location, String> locationDao;

    public TransactionController(Database database) {
        super(database);

        transactionDao = database.getTransactionDao();
        currencyDao    = database.getCurrencyDao();
        userDao        = database.getUserDao();
        categoryDao    = database.getCategoryDao();
        personDao      = database.getPersonDao();
        locationDao    = database.getLocationDao();
    }

    protected CrudResource getResourceDefinition() {
        return CrudResource.TRANSACTION;
    }

    protected Dao<Transaction,Integer> getEntityDao() {
        return transactionDao;
    }

    protected Integer parseEntityId(String id) {
        return Integer.parseInt(id);
    }

    protected Transaction setupEntityFromParams(Request req, Integer id, Transaction transaction) {
        if(transaction == null) {
            transaction = new Transaction();
            transaction.setDate(new Date());
        }

        try {
            final double amount = Double.parseDouble(getQueryParam(req, "amount"));
            transaction.setAmount(amount);

            final String currencyName = getQueryParam(req, "currency");
            final Currency currency = queryForExistingId(currencyDao, currencyName);
            transaction.setCurrency(currency);

            transaction.setEuroAmount(currency.toEuro(amount));

            final String userName = getQueryParam(req, "user");
            final User user = queryForExistingId(userDao, userName);
            transaction.setUser(user);

            final String categoryName = getQueryParam(req, "category");
            Category category = categoryDao.queryForId(categoryName);
            if(category == null) {
                category = new Category(categoryName);
                categoryDao.create(category);
            }
            transaction.setCategory(category);

            if(!req.queryParams("person").isEmpty()) {
                final int personId = Integer.parseInt(getQueryParam(req, "person"));
                final Person person = queryForExistingId(personDao, personId);
                transaction.setPerson(person);
            }

            if(!req.queryParams("location").isEmpty()) {
                final String locationName = getQueryParam(req, "location");
                final Location location = queryForExistingId(locationDao, locationName);
                transaction.setLocation(location);
            }
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }

        return transaction;
    }

    protected void setupEditModel(Map<String, Object> model, Transaction transaction) {
        try {
            model.put("currencies", currencyDao.queryForAll());
            model.put("users",      userDao.queryForAll());
            model.put("categories", categoryDao.queryForAll());
            model.put("persons",    personDao.queryForAll());
            model.put("locations",  locationDao.queryForAll());
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }

        if(transaction != null) {
            model.put("amount", transaction.getAmount());
            model.put("currency", transaction.getCurrency());
            model.put("user", transaction.getUser());
            model.put("category", transaction.getCategory());

            final Person person = transaction.getPerson();
            if(person != null)
                model.put("person", person);

            final Location location = transaction.getLocation();
            if(location != null)
                model.put("location", location);
        }
    }

    @Override
    protected void setupListModel(Map<String, Object> model) {
        try {
            final List<Transaction> transactions = transactionDao.queryForAll();
            final List<Map<String, Object>> transactionModels = new ArrayList<>();
            for(Transaction transaction : transactions) {
                final Map<String, Object> transactionModel = new HashMap<>();

                transactionModel.put("id", transaction.getId());
                transactionModel.put("amount", transaction.getAmount());
                transactionModel.put("currency", transaction.getCurrency());
                transactionModel.put("user", transaction.getUser());
                transactionModel.put("category", transaction.getCategory());

                final Person person = transaction.getPerson();
                if(person != null) {
                    transactionModel.put("person", person);
                    System.out.println(person);
                }

                final Location location = transaction.getLocation();
                if(location != null)
                    transactionModel.put("location", location);

                final DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance(
                    DateFormat.SHORT,
                    DateFormat.SHORT,
                    Locale.GERMAN);
                transactionModel.put("date", dateFormat.format(transaction.getDate()));

                transactionModels.add(transactionModel);
            }
            model.put("entities", transactionModels);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
