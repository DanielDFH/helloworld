package de.swt.geldboerse.config;

public interface WebServerConfig {

    String host();

    int port();
}
