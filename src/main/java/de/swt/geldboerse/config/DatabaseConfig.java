package de.swt.geldboerse.config;

public interface DatabaseConfig {

    String name();

    boolean createTables();

    boolean setupDummyData();
}
