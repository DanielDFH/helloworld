package de.swt.geldboerse.util;

public class Validation {

    public static boolean isValidId(String id) {
        if (id == null || id.isEmpty()) return false;        
        return true;
    }
    public static boolean isValidPassword(String password) {
        return password != null && !password.isEmpty();
    }
    
    public static boolean isValidPasswordCheck(String password, String passwordcheck) {
        return password.equals(passwordcheck);
    }
}
