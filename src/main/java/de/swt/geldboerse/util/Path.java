package de.swt.geldboerse.util;

/**
 * Various path constants go here.
 */
public class Path {
    public static class Web {
        public static final String LOGIN = "/login/";
        public static final String REGISTER = "/register/";
        public static final String DEFAULT = CrudResource.TRANSACTION.url();
        public static final String EXAMPLE = "/example/";
    }

    public static class Template {
        public static final String BASE_DIR = "templates";
        public static final String MODAL_MESSAGE = "pages/modal_message.mustache";
        public static final String EXAMPLE  = "pages/example.mustache";
        public static final String LOGIN    = "pages/login.mustache";
        public static final String REGISTER = "pages/register.mustache";
    }
}
