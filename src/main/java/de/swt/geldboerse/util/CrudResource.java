package de.swt.geldboerse.util;

public enum CrudResource {
    CURRENCY("currencies"),
    LOCATION("locations"),
    PERSON("persons"),
    TRANSACTION("transactions"),
    USER("users");

    private String plural;

    private CrudResource(String plural) {
        this.plural = plural;
    }

    public String singular() {
        return name().toLowerCase();
    }

    public String plural() {
        return plural;
    }

    private static String capitalized(String s) {
        final String firstChar = s.substring(0, 1);
        final String remainder = s.substring(1);
        return firstChar.toUpperCase() + remainder.toLowerCase();
    }

    public String capitalizedSingular() {
        return capitalized(singular());
    }

    public String capitalizedPlural() {
        return capitalized(plural());
    }

    public String url() {
        return "/"+plural+"/";
    }

    public String newEntityFormUrl() {
        return url()+"new/";
    }

    public String entityUrl(String id) {
        return url()+id+"/";
    }
}
