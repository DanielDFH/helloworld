package de.swt.geldboerse.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Filter;
import spark.Request;
import spark.Response;

public class Filters {
    private static Logger LOGGER = LoggerFactory.getLogger(Filters.class);

    public static Filter logResponse = (Request req, Response res) -> {
        LOGGER.info("[{}] {} {}", res.status(), req.requestMethod(), req.pathInfo());
    };

    public static Filter addTrailingSlashes = (Request req, Response res) -> {
        if (!req.pathInfo().endsWith("/")) {
            res.redirect(req.pathInfo() + "/");
        }
    };

    public static Filter addLoginCheck = (Request req, Response res) -> {
        final String path = req.pathInfo();
        final boolean atLoginOrRegistration =
            path.equals(Path.Web.LOGIN) ||
            path.equals(Path.Web.REGISTER);

        if (req.session().attribute("user") == null && !atLoginOrRegistration) {
            System.out.println("REDIRECT DUE TO MISSING USER");
            res.redirect(Path.Web.LOGIN);
        }
    };

    public static Filter redirectIfLoggedIn = (Request req, Response res) -> {
        if (req.session().attribute("user") != null) {
            res.redirect(Path.Web.DEFAULT);
        }
    };

    public static Filter redirectIfNotLoggedIn = (Request req, Response res) -> {
        if (req.session().attribute("user") == null) {
            res.redirect(Path.Web.DEFAULT);
        }
    };
}