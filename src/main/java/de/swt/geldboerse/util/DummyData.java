package de.swt.geldboerse.util;

import com.j256.ormlite.dao.Dao;
import de.swt.geldboerse.Database;
import de.swt.geldboerse.model.*;
import de.swt.geldboerse.model.Currency;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DummyData {
    public static void setup(Database database) {
        new DummyData(database);
    }

    private static List<String> firstNames = Arrays.asList(
        "Arne",
        "Leonie",
        "Leon",
        "Maike",
        "Bernd",
        "Kerstin",
        "Christoph",
        "Johanna",
        "Jan",
        "Erika",
        "Max",
        "Lisa",
        "Stefan",
        "Andrea",
        "Daniel",
        "Susanne",
        "Dennis",
        "Isabell",
        "Julian",
        "Tanja",
        "Frank",
        "Annika",
        "Felix",
        "Catharina",
        "Günther",
        "Verena",
        "Hannes",
        "Anna"
    );

    private static List<String> lastNames = Arrays.asList(
        "Müller",
        "Horstmann",
        "Quandt",
        "Siems",
        "Meyer",
        "Schwartz",
        "Noll",
        "Ostrowski",
        "Richter",
        "Sandoval",
        "Brückmann",
        "Wieland",
        "Novak",
        "Zimmer",
        "Kowalski",
        "Büchner",
        "Deckert",
        "Hoffmann",
        "Bittner",
        "Martens",
        "Malek"
    );

    private static List<String> domains = Arrays.asList(
        "aol.com",
        "fh-luebeck.de",
        "laudert.de",
        "di-unternehmer.com",
        "mail.de",
        "tu-darmstadt.de",
        "gmail.com",
        "yahoo.com",
        "web.de",
        "mail.ru",
        "me.com",
        "gmx.com"
    );

    private static List<String> streetName = Arrays.asList(
        "Grevener Straße",
        "Piusallee",
        "Am Knick",
        "Am Burloh",
        "Enschedeweg",
        "Hirseweg",
        "Hoove",
        "Teutonenstraße",
        "Parkallee",
        "Eulenbergstraße",
        "Holunderweg",
        "Erikahof",
        "Zur Wiese",
        "Friesenring",
        "Herrmann-Treff-Weg",
        "Am Brook",
        "Hindenburgplatz",
        "Ulmenweg",
        "Gartenstraße"
    );

    private static List<String> cityName = Arrays.asList(
        "Hamburg",
        "Reinfeld",
        "Bad Oldesloe",
        "Ahrensburg",
        "Lübeck",
        "Leipzig",
        "Mönchberg",
        "Lichtenau",
        "Weißbach",
        "Schönefeld",
        "Meckesheim",
        "Nürnberg",
        "Talheim",
        "Lautenbach",
        "Groß Sarau",
        "Kochel am See",
        "Mötzen",
        "Hagen am Teutoburger Wald",
        "Heidenheim an der Brenz",
        "Weiden"
    );

    // XZYmarkt
    // Zum abc XZY
    // Der XYZ
    // Die XYZ
    //
    // XYZbar
    // XYZ Club
    //

    private static List<String> locationNames = Arrays.asList(
        "Post",
        "Bar",
        "Aldi",
        "Famila",
        "Lidl",
        "Disko",
        "Edeka",
        "K+K",
        "Mediamarkt",
        "Citti Markt",
        "Schlecker", // :D
        "Restaurant",
        "Wochenmarkt",
        "Buchhandlung",
        "Frisör",
        "Subway",
        "Burger King",
        "Nordsee",
        "Club"
    );

    private static List<String> currencyNames = Arrays.asList(
        "Lira",
        "Yen",
        "Dollar",
        "Shekel",
        "Rubel",
        "Rupie",
        "Zloty",
        "Dinar", // :D
        "Pfennig",
        "Drachme",
        "Bitcoin",
        "Shitcoin",
        "Rinder",
        "Kühe",
        "Steinräder",
        "Sesterz",
        "Kronen",
        "Taler",
        "Dukaten"
    );

    private static List<String> categories = Arrays.asList(
        "Spaß",
        "Freizeit",
        "Unterhaltung",
        "Luxus",
        "Schule",
        "Studium",
        "Arbeit",
        "Miete",
        "Wohnung",
        "Garten",
        "Haus",
        "Familie",
        "Freunde",
        "Auto",
        "Transport",
        "Bus",
        "Bahn",
        "Post",
        "Nahrung",
        "Investition"
    );

    private static List<String> separators = Arrays.asList(
        ".",
        "-",
        "_",
        ""
    );

    private Random random;
    private Dao<Transaction, Integer> transactionDao;
    private Dao<Currency, String> currencyDao;
    private Dao<User, String> userDao;
    private Dao<Category, String> categoryDao;
    private Dao<Person, Integer> personDao;
    private Dao<Location, String> locationDao;

    private DummyData(Database database) {
        random = new Random();
        transactionDao = database.getTransactionDao();
        currencyDao    = database.getCurrencyDao();
        userDao        = database.getUserDao();
        categoryDao    = database.getCategoryDao();
        personDao      = database.getPersonDao();
        locationDao    = database.getLocationDao();

        for(int i = 0; i < 10; i++)
            generateCategory();
        for(int i = 0; i < 10; i++)
            generateUser();
        for(int i = 0; i < 20; i++)
            generatePerson();
        for(int i = 0; i < 10; i++)
            generateLocation();
        for(int i = 0; i < 10; i++)
            generateCurrency();
    }

    private String pickRandomlyFrom(List<String> list) {
        return list.get(random.nextInt(list.size()));
    }

    private static String normalize(String s) {
        return s.replace("ä", "ae")
                .replace("ö", "oe")
                .replace("ü", "ue")
                .replace("ß", "ss");
    }

    private Category generateCategory() {
        String name;

        try {
            do {
                name = pickRandomlyFrom(categories);
            } while(categoryDao.queryForId(name) != null);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }

        final Category category = new Category();
        category.setName(name);

        try {
            categoryDao.create(category);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }

        return category;
    }

    private User generateUser() {
        String firstName;
        String lastName;
        String name;

        try {
            do {
                firstName = normalize(pickRandomlyFrom(firstNames).toLowerCase());
                lastName  = normalize(pickRandomlyFrom(lastNames).toLowerCase());
                name = firstName + pickRandomlyFrom(separators) + lastName;
            } while(userDao.queryForId(name) != null);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }

        final String email = firstName.substring(0,1) +
                             pickRandomlyFrom(separators) +
                             lastName +
                             "@" +
                             pickRandomlyFrom(domains);
        final User user = new User(name, "1234", email);

        try {
            userDao.create(user);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }

        return user;
    }

    private Person generatePerson() {
        final Person person = new Person(pickRandomlyFrom(firstNames),
                                         pickRandomlyFrom(lastNames));
        try {
            personDao.create(person);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
        return person;
    }

    private Location generateLocation() {
        String name;
        try {
            do {
                name = pickRandomlyFrom(locationNames);
            } while(locationDao.queryForId(name) != null);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }

        final String street = String.format("%s %d%s %s",
            pickRandomlyFrom(streetName),
            1 + random.nextInt(70), // street number
            (random.nextInt(8) == 0) ?
                Character.toString((char)('a' + random.nextInt(20))) :
                "", // street number extra
            pickRandomlyFrom(cityName));
        final String postcode = Integer.toString(10000 + random.nextInt(90000)); // between 10000 and 89999
        final String comment = "";
        final Location location = new Location(name, street, postcode, comment);
        try {
            locationDao.create(location);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
        return location;
    }

    private Currency generateCurrency() {
        String name;
        try {
            do {
                name = pickRandomlyFrom(currencyNames);
            } while(currencyDao.queryForId(name) != null);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }

        final Currency currency = new Currency(name, (double)random.nextInt(300) / 100.0);
        try {
            currencyDao.create(currency);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
        return currency;
    }
}
