package de.swt.geldboerse.util;

import de.swt.geldboerse.Database;
import de.swt.geldboerse.controller.BasicController;
import spark.Request;

import java.util.HashMap;
import java.util.Map;

public class ModalMessageBuilder {
    private static class Controller extends BasicController {
        public Controller(Database database) {
            super(database);
        }

        @Override
        public void setup() {}

        public String render(Request req, Map<String, Object> model, String templatePath) {
            return super.render(req, model, templatePath);
        }
    }

    private static Controller controller;

    public static void setup(Database database) {
        controller = new Controller(database);
        controller.setup();
    }

    private String title;
    private String description;
    private String icon;
    private String targetUrl;

    public ModalMessageBuilder() {
        title = "";
        description = "";
        icon = "exclamation triangle";
        targetUrl = Path.Web.DEFAULT;
    }

    public ModalMessageBuilder title(String title) {
        this.title = title;
        return this;
    }

    public ModalMessageBuilder description(String description) {
        this.description = description;
        return this;
    }

    public ModalMessageBuilder icon(String icon) {
        this.icon = icon;
        return this;
    }

    public ModalMessageBuilder targetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
        return this;
    }

    public String render(Request req) {
        Map<String, Object> model = new HashMap<>();
        model.put("title", title);
        model.put("icon", icon);
        model.put("description", description);
        model.put("targetUrl", targetUrl);
        return controller.render(req, model, Path.Template.MODAL_MESSAGE);
    }
}
