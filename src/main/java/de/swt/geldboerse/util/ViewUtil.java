package de.swt.geldboerse.util;

import de.swt.geldboerse.Application;
import org.eclipse.jetty.http.HttpStatus;
import spark.ModelAndView;
import spark.Request;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import spark.Response;
import spark.Route;
import spark.template.mustache.MustacheTemplateEngine;

public class ViewUtil {
    public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);

    private static MustacheTemplateEngine templateEngine =
        new MustacheTemplateEngine(Path.Template.BASE_DIR);

    public static String render(Request req, Map<String, Object> model, String templatePath) {
        model.put("session", req.session());
        model.put("Application", Application.class); // Access to Dao objects
        model.put("WebPath", Path.Web.class); // Access application URLs from templates
        return templateEngine.render(new ModelAndView(model, templatePath));
    }

    public static Route notFound = (Request req, Response res) -> {
        res.status(HttpStatus.NOT_FOUND_404);
        return new ModalMessageBuilder()
            .title("'"+req.pathInfo()+"' does not exist")
            .description("Seems like you took the wrong turn - the requested resource is not available (anymore).")
            .icon("question circle")
            .render(req);
    };

    public static Route internalServerError = (Request req, Response res) -> {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
        return new ModalMessageBuilder()
            .title("Internal Server Error")
            .description("Something went wrong. :(")
            .icon("exclamation triangle")
            .render(req);
    };
}
