package de.swt.geldboerse.util;

public class RestResource {
    private String plural;

    public RestResource(String plural) {
        this.plural = plural;
    }

    public String url() {
        return "/"+this.plural+"/";
    }

    public String newEntityFormUrl() {
        return url()+"new/";
    }

    public String entityUrl(String id) {
        return url()+id+"/";
    }
}
