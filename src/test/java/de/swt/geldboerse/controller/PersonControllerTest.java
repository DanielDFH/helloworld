package de.swt.geldboerse.controller;

import de.swt.geldboerse.TestEnvironment;
import de.swt.geldboerse.model.Person;
import de.swt.geldboerse.util.CrudResource;
import net.jcip.annotations.NotThreadSafe;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("integration")
@NotThreadSafe
public class PersonControllerTest {
    private TestEnvironment env;
    private WebDriver web;

    @BeforeEach
    public void beforeEach() {
        env = new TestEnvironment();
        web = env.webDriver;
        env.loginWithDummyUser();
    }

    @AfterEach
    public void afterEach() {
        env.close();
    }

    @Test
    public void create() {
        String url = env.getBaseUrl()+CrudResource.PERSON.newEntityFormUrl();
        web.get(url);

        web.findElement(By.name("firstName")).sendKeys("Max");
        web.findElement(By.name("lastName")).sendKeys("Mustermann");
        web.findElement(By.className("form")).submit();

        assertEquals(CrudResource.PERSON.url(), env.getCurrentPath());
    }

    @Test
    public void read() throws SQLException {
        env.getDatabase().getPersonDao().create(new Person("Max", "Mustermann"));

        String url = env.getBaseUrl()+CrudResource.PERSON.url();
        web.get(url);

        boolean found = false;
        for(WebElement column : web.findElements(By.cssSelector("table td"))) {
            if(column.getText().equals("Max")) {
                found = true;
            }
        }
        assertTrue(found);
    }

    @Test
    public void update() throws SQLException {
        Person person = new Person("Max", "Mustermann");
        env.getDatabase().getPersonDao().create(person);
        final Integer id = env.getDatabase().getPersonDao().extractId(person);

        String url = env.getBaseUrl()+CrudResource.PERSON.entityUrl(id.toString());
        web.get(url);

        final WebElement firstName = web.findElement(By.name("firstName"));

        assertEquals("Max", firstName.getAttribute("value"));

        firstName.clear();
        firstName.sendKeys("Bernd");

        web.findElement(By.className("form")).submit();

        person = env.getDatabase().getPersonDao().queryForId(id);
        assertEquals("Bernd", person.getFirstName());
    }

    @Test
    public void delete() throws SQLException {
        Person person = new Person("Max", "Mustermann");
        env.getDatabase().getPersonDao().create(person);
        final Integer id = env.getDatabase().getPersonDao().extractId(person);

        String url = env.getBaseUrl()+CrudResource.PERSON.entityUrl(id.toString())+"delete/";
        web.get(url);

        person = env.getDatabase().getPersonDao().queryForId(id);
        assertNull(person);
    }
}
