package de.swt.geldboerse.controller;

import de.swt.geldboerse.TestEnvironment;
import de.swt.geldboerse.model.User;
import de.swt.geldboerse.util.CrudResource;
import net.jcip.annotations.NotThreadSafe;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("integration")
@NotThreadSafe
public class UserControllerTest {
    private TestEnvironment env;
    private WebDriver web;

    @BeforeEach
    public void beforeEach() {
        env = new TestEnvironment();
        web = env.webDriver;
        env.loginWithDummyUser();
    }

    @AfterEach
    public void afterEach() {
        env.close();
    }

    @Test
    public void create() {
        String url = env.getBaseUrl()+CrudResource.USER.newEntityFormUrl();
        web.get(url);

        web.findElement(By.name("id")).sendKeys("max.mustermann");
        web.findElement(By.name("email")).sendKeys("maxmustermann@example.org");
        web.findElement(By.name("password")).sendKeys("letmein");
        web.findElement(By.className("form")).submit();

        assertEquals(CrudResource.USER.url(), env.getCurrentPath());
    }

    @Test
    public void read() throws SQLException {
        env.getDatabase().getUserDao().create(new User("max.mustermann","letmein", "maxmustermann@example.org"));

        String url = env.getBaseUrl()+CrudResource.USER.url();
        web.get(url);

        boolean found = false;
        for(WebElement column : web.findElements(By.cssSelector("table td"))) {
            if(column.getText().equals("max.mustermann")) {
                found = true;
            }
        }
        assertTrue(found);
    }

    @Test
    public void update() throws SQLException {
        User user = new User("max.mustermann","letmein", "maxmustermann@example.org");
        final String oldPasswordHash = user.getPasswordHash();
        env.getDatabase().getUserDao().create(user);

        String url = env.getBaseUrl()+CrudResource.USER.entityUrl("max.mustermann");
        web.get(url);

        final WebElement passwordInput = web.findElement(By.name("password"));

        //assertEquals("max.mustermann", web.findElement(By.name("id")).getAttribute("value"));
        assertEquals("maxmustermann@example.org", web.findElement(By.name("email")).getAttribute("value"));
        assertEquals("", web.findElement(By.name("password")).getAttribute("value"));

        passwordInput.clear();
        passwordInput.sendKeys("123abc");

        web.findElement(By.className("form")).submit();

        user = env.getDatabase().getUserDao().queryForId("max.mustermann");
        System.out.println(oldPasswordHash+" <> "+user.getPasswordHash());
        assertTrue(!oldPasswordHash.equals(user.getPasswordHash())); // hash should've changed
    }

    @Test
    public void delete() throws SQLException {
        env.getDatabase().getUserDao().create(new User("max.mustermann","letmein", "maxmustermann@example.org"));

        String url = env.getBaseUrl()+CrudResource.USER.entityUrl("max.mustermann")+"delete/";
        web.get(url);

        final User user = env.getDatabase().getUserDao().queryForId("max.mustermann");
        assertNull(user);
    }
}
