package de.swt.geldboerse.controller;

import de.swt.geldboerse.TestEnvironment;
import de.swt.geldboerse.model.Currency;
import de.swt.geldboerse.util.CrudResource;
import net.jcip.annotations.NotThreadSafe;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("integration")
@NotThreadSafe
public class CurrencyControllerTest {
    private TestEnvironment env;
    private WebDriver web;

    @BeforeEach
    public void beforeEach() {
        env = new TestEnvironment();
        web = env.webDriver;
        env.loginWithDummyUser();
    }

    @AfterEach
    public void afterEach() {
        env.close();
    }

    @Test
    public void create() {
        String url = env.getBaseUrl()+CrudResource.CURRENCY.newEntityFormUrl();
        web.get(url);

        web.findElement(By.name("id")).sendKeys("Taler");
        web.findElement(By.name("euroFactor")).sendKeys("2.34");
        web.findElement(By.className("form")).submit();

        assertEquals(CrudResource.CURRENCY.url(), env.getCurrentPath());
    }

    @Test
    public void read() throws SQLException {
        env.getDatabase().getCurrencyDao().create(new Currency("Taler",2.34));

        String url = env.getBaseUrl()+CrudResource.CURRENCY.url();
        web.get(url);

        boolean found = false;
        for(WebElement column : web.findElements(By.cssSelector("table td"))) {
            if(column.getText().equals("Taler")) {
                found = true;
            }
        }
        assertTrue(found);
    }

    @Test
    public void update() throws SQLException {
        env.getDatabase().getCurrencyDao().create(new Currency("Taler",2.34));

        String url = env.getBaseUrl()+CrudResource.CURRENCY.entityUrl("Taler");
        web.get(url);

        final WebElement factorInput = web.findElement(By.name("euroFactor"));

        assertEquals("Taler", web.findElement(By.name("id")).getAttribute("value"));
        assertEquals("2.34", factorInput.getAttribute("value"));

        factorInput.clear();
        factorInput.sendKeys("13.37");

        web.findElement(By.className("form")).submit();

        final Currency currency = env.getDatabase().getCurrencyDao().queryForId("Taler");
        assertEquals(13.37, currency.getEuroFactor());
    }

    @Test
    public void delete() throws SQLException {
        env.getDatabase().getCurrencyDao().create(new Currency("Taler",2.34));

        String url = env.getBaseUrl()+CrudResource.CURRENCY.entityUrl("Taler")+"delete/";
        web.get(url);

        final Currency currency = env.getDatabase().getCurrencyDao().queryForId("Taler");
        assertNull(currency);
    }
}
