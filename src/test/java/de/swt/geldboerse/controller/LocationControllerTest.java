package de.swt.geldboerse.controller;

import de.swt.geldboerse.TestEnvironment;
import de.swt.geldboerse.model.Location;
import de.swt.geldboerse.util.CrudResource;
import net.jcip.annotations.NotThreadSafe;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Tag("integration")
@NotThreadSafe
public class LocationControllerTest {
    private TestEnvironment env;
    private WebDriver web;

    @BeforeEach
    public void beforeEach() {
        env = new TestEnvironment();
        web = env.webDriver;
        env.loginWithDummyUser();
    }

    @AfterEach
    public void afterEach() {
        env.close();
    }

    @Test
    public void create() {
        String url = env.getBaseUrl()+CrudResource.LOCATION.newEntityFormUrl();
        web.get(url);

        web.findElement(By.name("id")).sendKeys("Bahnhof");
        web.findElement(By.name("street")).sendKeys("Bahnhofstraße");
        web.findElement(By.name("postcode")).sendKeys("1234");
        web.findElement(By.name("comment")).sendKeys("testtest");
        web.findElement(By.className("form")).submit();

        assertEquals(CrudResource.LOCATION.url(), env.getCurrentPath());
    }

    @Test
    public void read() throws SQLException {
        env.getDatabase().getLocationDao().create(new Location("Bahnhof", "Bahnhofstraße", "1234", "testtest"));

        String url = env.getBaseUrl()+CrudResource.LOCATION.url();
        web.get(url);

        boolean found = false;
        for(WebElement column : web.findElements(By.cssSelector("table td"))) {
            if(column.getText().equals("Bahnhof")) {
                found = true;
            }
        }
        assertTrue(found);
    }

    @Test
    public void update() throws SQLException {
        env.getDatabase().getLocationDao().create(new Location("Bahnhof", "Bahnhofstraße", "1234", "testtest"));

        String url = env.getBaseUrl()+CrudResource.LOCATION.entityUrl("Bahnhof");
        web.get(url);

        final WebElement postcodeInput = web.findElement(By.name("postcode"));

        assertEquals("Bahnhof", web.findElement(By.name("id")).getAttribute("value"));
        assertEquals("1234", postcodeInput.getAttribute("value"));

        postcodeInput.clear();
        postcodeInput.sendKeys("9876");

        web.findElement(By.className("form")).submit();

        final Location location = env.getDatabase().getLocationDao().queryForId("Bahnhof");
        assertEquals("9876", location.getPostcode());
    }

    @Test
    public void delete() throws SQLException {
        env.getDatabase().getLocationDao().create(new Location("Bahnhof", "Bahnhofstraße", "1234", "testtest"));

        String url = env.getBaseUrl()+CrudResource.LOCATION.entityUrl("Bahnhof")+"delete/";
        web.get(url);

        final Location currency = env.getDatabase().getLocationDao().queryForId("Bahnhof");
        assertNull(currency);
    }
}
