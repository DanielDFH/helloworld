package de.swt.geldboerse;

import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.j256.ormlite.table.TableUtils;
import de.swt.geldboerse.model.User;
import de.swt.geldboerse.util.Path;
import org.apache.commons.logging.LogFactory;
import org.cfg4j.provider.ConfigurationProvider;
import org.cfg4j.provider.ConfigurationProviderBuilder;
import org.cfg4j.source.inmemory.InMemoryConfigurationSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import spark.Spark;

import java.sql.SQLException;
import java.util.Properties;

public class TestEnvironment implements AutoCloseable {
    public WebDriver webDriver;
    private Application application;
    private String webServerHost;

    public TestEnvironment() {
        webServerHost = "127.0.0.1";
        webDriver = new HtmlUnitDriver(true) { // JavaScript is required by some pages
            protected WebClient modifyWebClient(WebClient client) {
                client.setCssErrorHandler(new SilentCssErrorHandler());
                // This is required because Semantic-UI somehow causes HtmlUnit to log tons of parsing errors.
                return client;
            }
        };
        application = new Application(createConfigurationProvider());
    }

    private ConfigurationProvider createConfigurationProvider() {
        final Properties properties = new Properties();
        properties.setProperty("database.name", ":memory:"); // In-memory database - see https://sqlite.org/uri.html
        properties.setProperty("database.createTables", "true");
        properties.setProperty("database.setupDummyData", "false");
        properties.setProperty("webserver.host", webServerHost);
        properties.setProperty("webserver.port", "0"); // Use an ephemeral port
        return new ConfigurationProviderBuilder()
            .withConfigurationSource(new InMemoryConfigurationSource(properties))
            .build();
    }

    public String getBaseUrl() {
        return String.format("http://%s:%d", webServerHost, Spark.port());
    }

    public String getCurrentPath() {
        final String baseUrl = getBaseUrl();
        final String url = webDriver.getCurrentUrl();
        if(!url.startsWith(baseUrl)) {
            throw new RuntimeException("Unexpected URL");
        }
        return url.substring(baseUrl.length());
    }

    public Database getDatabase() {
        return application.getDatabase();
    }

    @Override
    public void close() {
        webDriver.quit();
        webDriver = null;
        application.close();
    }

    public User loginWithDummyUser() {
        final User user = new User("dummy", "1234", "dummy@example.org");
        try {
            application.getDatabase().getUserDao().create(user);
        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
        webDriver.get(getBaseUrl()+Path.Web.LOGIN);
        webDriver.findElement(By.name("user")).sendKeys("dummy");
        webDriver.findElement(By.name("password")).sendKeys("1234");
        webDriver.findElement(By.className("form")).submit();
        return user;
    }
}
