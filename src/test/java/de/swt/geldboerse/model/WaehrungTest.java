
package de.swt.geldboerse.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WaehrungTest {
    Currency aaa;
    Currency bbb;

    @BeforeEach
    public void beforeEach() {
        aaa  = new Currency("aaa", 2.0);
        bbb = new Currency("bbb", 0.5);
    }

    @Test
    public void testInEuro() {
        assertEquals(50.00, aaa.toEuro(100.00));
        assertEquals(100.00, bbb.toEuro(50.00));
    }

    @Test
    public void testVonEuro() {
        assertEquals(100.00, aaa.fromEuro(50.00));
        assertEquals(50.00, bbb.fromEuro(100));
    }

    @Test
    public void testUmrechnen() {
        assertEquals(400.00, aaa.convert(100.00, bbb));
    }

    @Test
    public void testGetName() {
        assertEquals("aaa", aaa.getName());
        assertEquals("bbb", bbb.getName());
    }

    @Test
    public void testSetName() {
        aaa.setName("ccc");
        bbb.setName("ddd");
        assertEquals("ccc", aaa.getName());
        assertEquals("ddd", bbb.getName());
    }

    @Test
    public void testGetEuroFaktor() {
        assertEquals(2.0, aaa.getEuroFactor());
        assertEquals(0.5, bbb.getEuroFactor());
    }

    @Test
    public void testSetEuroFaktor() {
        aaa.setEuroFactor(0.3);
        bbb.setEuroFactor(3.0);
        assertEquals(0.3, aaa.getEuroFactor());
        assertEquals(3.0, bbb.getEuroFactor());
    }

    @Test
    public void testToString() {
        assertEquals("aaa", aaa.toString());
        assertEquals("bbb", bbb.toString());
    }
}
