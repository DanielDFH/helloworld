package de.swt.geldboerse.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Arrays;
import java.util.List;

public class TransaktionTest {
    Transaction transaction;

    @BeforeEach
    public void beforeEach() {
        final Currency talerCurrency = new Currency("Taler", 0.5);
        final double talerAmount = 200.0;
        transaction = new Transaction(
            talerAmount,
            talerAmount * talerCurrency.getEuroFactor(), // amount in Euros
            talerCurrency,
            Arrays.asList(new Category("Kuchen")), // categories
            new Location("Irgendwo"));
    }

    @Test
    public void testHasPerson() {
        final Currency talerCurrency = new Currency("Taler", 0.5);
        final double talerAmount = 200.0;
        Transaction leihTransaction = new Transaction(
            talerAmount,
            talerAmount * talerCurrency.getEuroFactor(), // amount in Euros
            talerCurrency,
            Arrays.asList(new Category("Kuchen")), // categories
            new Person("Max", "Mustermann"));

        assertFalse(transaction.hasPerson());
        assertTrue(leihTransaction.hasPerson());
    }

    @Test
    public void testGetBetrag() {
        assertEquals(200, transaction.getAmount());
    }

    @Test
    public void testGetters() {
        final Currency talerCurrency = new Currency("Taler", 0.5);
        final double talerAmount = 200.0;
        final List<Category> kategorienAsList = Arrays.asList(new Category("Kuchen"));
        final Location location = new Location("Irgendwo");
        transaction = new Transaction(talerAmount, talerCurrency.toEuro(talerAmount), talerCurrency, kategorienAsList, location);

        assertEquals(talerAmount, transaction.getAmount());
        assertEquals(kategorienAsList, transaction.getCategoriesAsList());
        assertEquals(talerCurrency.toEuro(talerAmount), transaction.getEuroAmount());
        assertEquals(location, transaction.getLocation());
    }

}
