package de.swt.geldboerse.model;

import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class KategorieTest {

    @Test
    public void testGetName() {
        final Category category = new Category("Kuchen");
        assertEquals("Kuchen", category.getName());
    }

    @Test
    public void testSetName() {
        final Category category = new Category("Kuchen");
        category.setName("Keks");
        assertEquals(category.getName(),"Keks");
    }

    @Test
    public void testToString() {
        final Category category = new Category("Kuchen");
        assertEquals("Kuchen", category.toString());
    }

}