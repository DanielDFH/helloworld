package de.swt.geldboerse.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTest {
    User user;

    @BeforeEach
    public void beforeEach() {
        user = new User();
        user.setAmount(100.0);
    }

    @Test
    public void allowsPositiveOrZeroCredit() {
        assertTrue(user.changeAmount(-50.0)); // = 50
        assertTrue(user.changeAmount(-50.0)); // =  0
    }

    @Test
    public void preventsNegativeCredit() {
        assertFalse(user.changeAmount(-200.0)); // = -100
    }

    @Test
    public void testToDouble() {
        assertEquals(0.50, user.toDouble("0,50"));
    }

    @Test
    public void testCheckInput() {
        assertTrue(user.checkInput("10,57"));
        assertTrue(user.checkInput("10.57"));
        assertTrue(user.checkInput("10"));
        assertFalse(user.checkInput(".123"));
    }

    @Test
    public void testGuthabenAendern() {
        assertTrue(user.changeAmount(100.00));
        assertEquals(200.00, user.getAmount());
        assertTrue(user.changeAmount(-100.00));
        assertEquals(100.00, user.getAmount());
    }
}
