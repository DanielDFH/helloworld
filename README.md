# Digitale Geldbörse SWT2

Virtuelles Portemonaie
von Daniel Dornhof, Joe Phoomkhonsan

Die Applikation benötigt eine Datenbankanbindung um zu laufen.

In der Klasse DatabaseMain befinden sich die Parameter für die Datenbank, welche gegebenfalls angepasst werden müssen.
Zum Testen muss eine MySQL Datenbank erstellt werden (Name:'swt') .
Die Applikation erstellt selbständig die Tabellen, sofern die Verbindungsparameter stimmen.

Über MainClass im Paket MainPackage wird die Applikation gestartet.
