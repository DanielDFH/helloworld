# Aufgabenstellung

Die Aufgabenstellung war ein Projekt, das schon spezifiziert ist (z.B. das entstandene Projekt im Rahmen des Softwaretechnik I Moduls)
nach folgenden Anforderungen / Deadlines weiter zu entwickeln:

- KW 16: Festlegung der Projekt-Technologien für die Weiterentwicklung
- KW 18: Einrichtungs des Automatisierungskonzepts CI Semaphore
- KW 20: Projekt in Issues eingeteilt und je Issue nach Aufwand geschätzt
- KW 22: Vorbereitung zu dem drei-wöchigen Sprint abgeschlossen. Definition of Done gewählt, Issues angelegt
	und abwechselnde Rollen der Gruppenteilnehmer festgelegt
- KW 25: Vorstellung der Demo-Version, die nach dem Sprint entstanden ist
- KW 27: Durchführung des Usability-Tests
- KW 28: Abschlusspräsentation der Testergebnisse
- KW 29: Abgabe der Dokumentation über den Projekt
