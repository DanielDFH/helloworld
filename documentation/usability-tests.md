# Usability-Tests
## Planung
### Leitfaden
#### Vor dem Test:
Vor dem Beginn der Usability-Tests wird auf alle Tester gewartet, während des Wartens wird die Testumgebung, für den Start der Usability-Tests, vorbereitet. Sobald alle Tester anwesend sind, wird das Projekt kurz erläutert und die Tester werden in ihre bevorstehenden Aufgaben eingeführt.
Den Testern wird erklärt, dass wir ihre Einverständniserklärung benötigen und weshalb wir diese benötigen. Daraufhin werden die Nutzer dazu aufgefordert ein Formular auszufüllen, um festzustellen was für Tester an unseren Usability-Tests teilgenommen haben und diese evtl. in Gruppen einteilen zu können. Dieses Formular dient auch als Einverständniserklärung.

#### Während des Tests:
Während der Usability-Tests wird ein Tester von den anderen separiert und einzeln darum gebeten eine Reihe von gegebenen Aufgaben zu lösen, welche alle Funktionen unserer Applikation umfasst.
Während der Bearbeitung dieser Aufgaben, wird der Tester durch die Webcam des Geräts und durch eine weitere Kamera gefilmt. Es werden zwei Kameras verwendet um Sachen festzuhalten die durch eine Kamera möglicherweise nicht aufgenommen werden konnten.
Der Tester wird gegeben falls nochmal, durch den Moderator, daraufhin gewiesen laut zu denken, damit die Kameras und der Protokollant, dies auch aufzeichnen können.
Bei Schwierigkeiten bzw. Fragen von dem Tester, hilft der Moderator aus. Der Moderator soll allerdings nicht den Test übernehmen.

#### Nach dem Test:
Nach dem Test sollen die Tester erneut einen Fragebogen ausfüllen, dieser wird allerdings dazu verwendet um festzustellen, wie zufrieden die Tester mit der Applikation und ihrem Umfang sind und ob die Tester noch weitere Vorschläge haben um diese Applikation zu verbessern.
Das Team bedankt sich bei den Testern und verabschiedet sich von ihnen.

Das Team sammelt, alle Ergebnisse und wertet diese aus.


### Zeitliche Planung:
Es sind für die Einführung in die Aufgaben und das Projekt 20 Minuten vorgesehen.
Die Zeit die für die Usability-Tests vorgesehen ist beträgt 15 Minuten pro Person.
Die benötigte Zeit für die Auswertung beträgt 15 Minuten pro Person (für die Videos) und 30 Minuten für die Auswertung anderer Resultate (Click-Tracker/ Frage-Bogen) und deren Interpretation.

#### Methoden:
Zum Auswerten der Tests verwenden wir unsere Aufnahmen, durch die Web-Cam des Geräts und durch unser anderes Aufnahmegerät. Abgesehen davon, verwenden wir das Protokoll über die auftretenden Schwierigkeiten während des Tests.

Um die Durchführung der Tests besser nachvollziehen zu können haben wir einen Click-Tracker in unsere Applikation eingebaut.

Um die Zufriedenheit der Tester, bezogen auf die Applikation, feststellen zu können haben wir einen Fragebogen verwendet.

## Auswertung
### Auswertung der Videos und des Protokolls:
Obwohl die Tester laut gesprochen haben, schien das laute Denken nicht sonderlich effektiv gewesen zu sein. Wir haben kaum Informationen aus den Gedanken, der Tester entnehmen können. Das einzige was uns an dieser Stelle auffiel ist, dass einige Tester Probleme hatten einige der englischen Begriffe in unserer GUI einordnen zu können (Die Aufgaben waren auf Deutsch gestellt und die GUI war auf Englisch).
Dies hat uns zu der Idee geführt, dass man die Sprache der GUI auf Deutsch setzen sollte oder gar nach Belieben zwischen Sprachen wechseln können sollte.

### Auswertung des Click-Trackers:
Anhand des Click-Trackers konnten wir feststellen, dass die Tester dazu tendierten auf ein dekoratives Icon in der oberen rechten Ecke zu klicken. Wir vermuten, dass dies getan wurde um auf eine Art Hauptbildschirm zu kommen, allerdings hat dies keine Funktion. Daraus resultiert für uns die Aufgabe diesem Icon eine Funktion zuzuteilen. Alternativ bestünde die Möglichkeit, dass Icon so zu gestalten, dass der Nutzer sofort sieht, dass es keine Funktion bietet.

![Clicktracker Klicks](clicktracker-clicks.png){ width=70% }

![Clicktracker Cursorbewegung](clicktracker-movement.png){ width=70% }


### Auswertung der Fragebögen:
Die Menge an Testern ist zu klein um diese in verschiedene Gruppen zu spalten.
Deswegen haben die Tester einer nach dem anderen die Software gestest.

Bevor der Softwaretest losging, haben die Tester den Fragebogen vor dem Test ausgefüllt.
In dem ersten Teil des Fragebogens vor dem Test ging es darum die Kontaktdaten zu erfassen und die Einverständniserklärung zu bekommen.
In dem zweiten Teil des Fragebogens wurden zwei Fragen zur Einschätzung der Tester gestellt. Diese zwei Fragen sind wie folgt ausgefallen:

| Fragen 	| Ja | Nein |
|:-------:	|:-:|:-:|
|Sind Sie in dem Umgang mit der Regestrierung, An- und Abmeldefunktionen der Online-Shops oder E-Mail-Accounts sicher?| 67% | 33% |
|   |   |   |
|Hatten Sie zuvor mit derart Software/App gearbeitet ggf. getestet?| 33% | 67% |

Folgende Ergebnisse wurden beim zweiten Fragebogen nach dem Softwaretest erfasst:

Bei den ersten vier Fragen hatten die Tester die Möglichkeit von 1 bis 5 zu beantworten. Wobei 1 für nicht zutreffend und 5 für sehr zutreffend stehen.

| Fragen 	| 1 | 2 | 3 | 4 | 5 |
|:-------:	|:-:|:-:|:-:|:-:|:-:|
|Die Software bietet alle Funktionen, um die anfallenden Aufgaben effizient zu bewältigen | - | - | - | 33% | 67% |
|   |   |   |   |   |   |
|Die Software erfordert keine überflüssigen Eingaben | - | - | - | - | 100% |
|   |   |   |   |   |   |
|Die Software ist gut auf die Anforderungen der Arbeit zugeschnitten | - | - | - | - | 100% |
|   |   |   |   |   |   |
|Die Software liefert in zureichendem Maße Informationen darüber, welche Eingaben zulässig oder nötig sind | - | - | - | - | 100% |

Bei den weiteren drei Fragen handelte es sich um die Ja/Nein fragen, die wie folgt beantwortet wurden:

| Fragen 	| Ja | Nein |
|:-------:	|:-:|:-:|
|Würden Sie die Applikation weiterhin verwenden?| 100% | - |
|   |   |   |
|Würden Sie die Applikation weiter empfehlen?| 100% | - |
|   |   |   |
|Wären Sie bereit für solch einen Services zu zahlen, wenn er etwas weiter ausgebaut wurde?| 33% | 67% |

Anhand der Fragebögen ist zu erkennen, dass die Tester grundsätzlich zufrieden sind mit der Applikation, allerdings nicht unbedingt für die Applikation, in dem Umfang, zahlen wollen würden.

