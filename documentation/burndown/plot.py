#!/usr/bin/env python3
import matplotlib.pyplot as plot
import matplotlib as mpl
import numpy
import pandas

data = pandas.read_csv('data.csv', delim_whitespace=True)

mpl.style.use('default')

figure, ax1 = plot.subplots()

ax1.plot('day', 'remaining_issues', 'C1.-', data=data, label='Reale Issues')
ax1.set_xlabel('Tage')
ax1.set_ylabel('Verbleibende Issues')

ax2 = ax1.twiny()
ax2.plot('day', 'ideal_remaining_issues', 'C2.-', data=data, label='Ideale Issues')

ax3 = ax1.twinx()
ax3.plot('day', 'remaining_story_points', 'C3', data=data, label='Story Points')
ax3.set_ylabel('Verbleibende Story Points')

#figure.tight_layout(rect=[0, 0, 0.75, 1])
figure.subplots_adjust(right=0.75)
figure.legend(loc='center right')
plot.show()
