---
title: Softwaretechnik II - Projektarbeit
subtitle: Gruppe 3Q
institute: Fachhochschule Lübeck
author:
- Luca Thurm
- Daniel Dornhof
- Eugen Kin
- Henry Kielmann
colorlinks: true
links-as-notes: true
lang: de-DE
otherlangs: [en-US]
toc: true
documentclass: scrartcl
papersize: a4
numbersections: true
secnumdepth: 3
---

<!--
Von Pandoc verarbeitete Metadaten werden hier definiert:
http://pandoc.org/MANUAL.html#variables-set-by-pandoc

Interessant:
- date
- toc-depth
- secnumdepth
- lof (list of figures)
- lot (list of tables)
- bibliography
- biblio-style
- biblio-title
- links-as-notes
-->
