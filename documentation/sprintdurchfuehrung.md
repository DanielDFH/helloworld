# Sprintdurchführung
## Woche 1 (28.5 - 3.6)
### Eigenleistung
#### Luca Thurm

Rolle: Project Owner / Entwickler
Ich habe mir in der ersten Woche das Issue#9 (Ausleihen/Verleihen) zugewiesen. Anfangs der Woche hatte ich Schwierigkeiten, das Projekt zu bearbeiten, da ein Fehler in der pom.xml-Datei existierte, welcher es mir unmöglich machte das Programm zum Laufen zu bekommen. Hauptsächlich habe ich mich in der ersten Woche in die verwendeten Technologien (Spark, Mustache) eingearbeitet und erste kleinere Änderungen der Portemonnaie-Klasse mit Hinblick auf das Aus-/Verleihen von Geld vorgenommen. Auch habe ich in meiner Rolle als Project Owner den Workflow, sowie die Definition of Done schriftlich digital formuliert und in das Repository eingegliedert und auch die Rollenverteilung für die nächsten beiden Sprintwochen vorgenommen.

Es ergab sich folgender Arbeitsaufwand:
- Auseinandersetzung mit dem Fehler in der pom.xml (2h)
- Einarbeiten in die Technologien (4h)
- Formulieren der Dokumente(1,75h)

Der gesamte Arbeitsaufwand der Woche betrug ca. 7,75h.


#### Daniel Dornhof

Überlegungen zur Datenbank. Hierbei war der Hauptaspekt der Überlegungen, ob wir ein SQL- oder NoSQL-Datenbanksystem verwenden. Ein weiterer Aspekt der Überlegungen waren die Hosting-Möglichkeiten und dem entgültigen Entschluss der Verwendung von [SQLite]. (ca. 5h)

Anbindung der Datenbank und benötigte Korrekturen im Code. (ca. 2h)

Der gesamte Arbeitsaufwand der Woche betrug ca. 7h.


#### Eugen Kin

Rolle: Tester / Entwickler.
Am Anfang der Woche habe ich mir keinen Issue zugewiesen,
da alle Issues mit hoher Priorität reserviert wurden und es noch nichts zu testen gab.
Erst zu Ende der Woche wurde bekanntgegeben, dass ein Teilnehmer voraussichtlich wegfällt und seine wichtigen Issues
(Issue#6- Nutzer registrieren; Issue#7 - User Login; Issue#8 - Nutzer Logout) nicht bearbeitet wurden.
Für diese Issues habe ich mich am Ende der Woche eingetragen.

Ziel in der ersten Woche war diese Issues im Model zu implimentieren.
Daraus entstand folgender Arbeitsaufwand:
- Model.User Klasse: Sign-Up und Login implementiert (1,5h)
- Auseinandersetzung und implementierung der Hashcode-Logik (Erstellung und Überprüfung) (4h)
- Auseinandersetzung mit dem Fehler in der Maven-Datei (2h)

Der gesamte Arbeitsaufwand der Woche betrug ca. 7,5h.

Das Ziel der Woche wurde zum größten Teil erfüllt. Model.User Klasse: Sign-Up und Login Implementierung war noch nicht komplett.


#### Henry Kielmann

Anstatt des ursprünglichen Desktop UIs ([AWT]) sollte ein Web-Server
eingesetzt werden.  Dazu wurde das [Spark Web-Framework][Spark], sowie die
[Mustache Template-Engine][mustache] in die Applikation integriert und
eine Beispiel-Seite inklusive Integrartions-Test implementiert.
Dies sollte als Referenz für die spätere Implementierung von konkreten Seiten
nach dem [MVC-Pattern][MVC] dienen. (8h)

Damit die Applikation ohne Rekompilierung an verschiedene Umgebungen angepasst
werden kann, wurde die Konfiguration mittels der [cfg4j Bibliothek][cfg4j] in
eine Datei ausgelagert. (1,5h)

Zudem wurde die Struktur der Dokumentation angelegt, sowie eine interne
Dokumentation erstellt, welche die genutzten Web-Technologien kurz erläuterte. (1h)

Der gesamte Arbeitsaufwand der Woche betrug 10,5h.


#### Rollenverteilung

- D. Dornhof - Datenbank
- E. Kin - Tester
- H. Kielmann - Build Engineer
- L. Thurm - Projektleiter

- ~~J. Phoomkhonsan - Tester~~
- ~~J. Wolf - Scrum Master~~


### Probleme und Lösungen
- Fehlen einer Datenbank und dem daraus resultierenden Zeitverlust durchs Warten um den Code zu testen.
    - Implementierung von SQLite Anbindung
- Maven Projekt lässt sich nicht richtig ausführen
    - Das Treffen der Gruppe wurde organisiert. Die Fehler wurden ermittelt und behoben.
- Wegfall von einem Teilnehmer. Die Issues mit hoher Priorität wurden nicht bearbeitet.
    - Nachdem diese Information bekannt war, wurden Issues: Login und Signup in Auftrag genommen.


### Reflektion über die Woche

Die erste Woche ist allgemein gut Verlaufen.
Die Ziele der ersten Woche konnten durch gute Aufteilung, effektive Plannung und eine vollständige Gruppe, zum größten Teil erreicht werden.
Beim Start der Woche sind ein Paar der erwähnten Probleme und Unklarheiten entstanden, die schnell gelöst werden konnten.


### Zusammenfassung

Die gesetzten Ziele der ersten Woche wurden erreicht.
Die Arbeit an den weiteren Zielen des Sprints wurde ermöglicht.


## Woche 2 (4. - 10.6)
### Eigenleistung
#### Luca Thurm

Rolle Scrum-Master, Entwickler
In der zweiten Woche habe ich mich weiterhin mit den Spark- und Mustache-Dokumentationen auseinandergesetzt. Auch habe ich Erste gößere Änderungen für Issue#9 gemacht.

Es ergab sich folgender Arbeitsaufwand:
- Einarbeiten in Spark und Mustache(5h)
- Änderungen im Code(1h)

Der Gesamte Arbeitsaufwand der Woche betrug ca. 6h


#### Daniel Dornhof

Entfernen von Autor-Kommentaren aus dem Code. (ca. 30 min)

Mergen von Datenbank-Klassen und Model-Klassen und aufkommende Fehler korrigieren, um das [MVC-Pattern][MVC] zu wahren. (ca. 6h)

Der gesamte Arbeitsaufwand der Woche betrug ca. 6,5h


#### Eugen Kin

Rolle: Entwickler
Die Bearbeitung mir zugewiesenen Issues (Issue#6- Nutzer registrieren; Issue#7 - User Login; Issue#8 - Nutzer Logout) wurder fortgesetzt.
Daraus entstand folgender Arbeitsaufwand:
- Model.User Klasse: Sign-Up und Login ergänzt (2h)
- Einarbeitung in das Projekt: Kennenlernen der Spark Methoden (4h)

Der gesamte Arbeitsaufwand der Woche belief sich auf 6h.

Das Ziel der Woche wurde nicht ganz Erfüllt,
da die Einarbeitung in die Spark Methoden viel Zeit genommen hat und es praktisch in UI diese Woche nichts umgesetzt wurde.
Die Model.User Klasse wurde soweit fertiggestellt, sodass man in der nächsten Woche gut loslegen kann.


#### Henry Kielmann

In dieser Woche wurden von mir lediglich Korrekturen am Build-System, der Versionverwaltung und kleinere Refaktorisierungen getätigt. (4h)

Zudem wurde die Dokumentation wurde etwas verbessert. (2h)

Der gesamte Arbeitsaufwand der Woche betrug 6h.


#### Rollenverteilung

- D. Dornhof - Tester
- E. Kin - Developer
- H. Kielmann - Projektleiter
- L. Thurm - Scrum Master

- ~~J. Phoomkhonsan - Build Engineer~~
- ~~J. Wolf - Tester~~


### Probleme und Lösungen
### Reflektion über die Woche

Aufgrund des endgültigen Ausfalls eines Gruppenmitglieds, ging die Organisation, der ersten Woche, nicht mehr auf.
Um weiterhin eine gute Aufteilung und eine effektive Planung einzuhalten, musste die Gruppe sich neu organisieren. Dies geschah beim Gruppen-Meeting der 2. Woche. Bei der Neuorganisation des Projekts, wurden die Pflicht-Issues beibehalten und erneut auf die einzelnen Mitglieder verteilt.


### Zusammenfassung

Diese Woche wurde verwendet um den Code anzupassen und zu verbessern.
Das Ziel der zweiten Woche wurde, aufgrund des Ausfalls eines der Gruppenmitglieder, nicht im Ausmaß der Planung erzielt.


## Woche 3 (11. - 17.6)
### Eigenleistung
#### Luca Thurm

Rolle: Tester / Entwickler
In dieser Woche war mir die Rolle Tester zugewiesen, weswegen ich mich hauptsächlich um die implementierung von Unit-Tests für die Klassen des Models gekümmert habe. Auch habe ich in der dritten Woche die Transaction- und Wallet-Klassen für das Ver-/Ausleihen von Geld (Issue#9) angepasst und kleinere Bugs im Model gefixt.

Es ergab sich folgender Arbeitsaufwand:
- Implementierung von Unit-Tests (3h)
- Abreit für Issue#9 (5h)
- Bugfixes (1h)

Der gesamte Arbeitsaufwand der Woche betrug ca. 9h


#### Daniel Dornhof

Einarbeitung in neue Technologien ([Semantic-UI][Semantic], [mustache] und [Spark]). (ca. 4h)
Erstellung von groben Web-Templates, mit [Semanitc-UI], um die Feinarbeit an diesen zu erleichtern. (ca. 4h)

Mitarbeit an Logik für die verschiedenen Seiten. (ca. 6h)

Der gesamte Arbeitsaufwand der Woche betrug ca. 14h


#### Eugen Kin

Rolle: Tester/Entwickler
Die Bearbeitung mir zugewiesenen Issues (Issue#6- Nutzer registrieren; Issue#7 - User Login; Issue#8 - Nutzer Logout) wurder fortgesetzt.
Daraus entstand folgender Arbeitsaufwand:
- Einarbeitung in neue Technologien ([Semantic-UI][Semantic], [mustache] und [Spark]) (4h)
- LoginController: angelegt und implementiert (3h)
- Ergänzung der zutreffenden Web-Templates mit Links und values (2h)
- Login und Signup über den Web Server ermöglicht (4h)

Der gesamte Arbeitsaufwand der Woche belief sich auf ca. 16h.

Das Ziel der Woche war es die UI fertig für die Demo-Vorführung zu stellen.
Leider wurde das Ziel nicht erfüllt, weil die entstandenen Merge-Konflikte erst kurze Zeit nach der Demo-Vorführung behoben wurden.
Die Änderungen wurden danach hochgeladen.


#### Henry Kielmann

Die für Integrationstests aufgesetzte Testumgebung wurde verbessert:

- Der Web-Server verwendet einen zufälligen verfügbaren Port, womit parallele
  Tests ermöglicht werden. (1h)
- Es wird jeweils eine frische In-Memory Datenbank aufgesetzt. (0,5h)

Es wurden Desktop spezifische, nun ungenutzte, Klassen aus dem Quellcode
entfernt.  Zudem wurden viele Variablen- und Methodennamen ins Englische
übersetzt. (1h)

Um bestimmte Entitäten, wie Beispielsweise Währungen, in der Web-Oberfläche
komfortabel verwalten zu können, wurden entsprechende Seiten angelegt.
Diese ermöglichen das Erstellen, Auslesen, Ändern und Löschen von Entitäten.
([CRUD]) (10h)

Für eine etwas bessere Nutzererfahrung im Falle von Fehlern, wurden Web-Seiten
angelegt welche dem Nutzer in diesem Falle angezeigt werden. (2h)

Der gesamte Arbeitsaufwand der Woche betrug 14,5h.


#### Rollenverteilung

- D. Dornhof - Build Engineer
- E. Kin - Tester
- H. Kielmann - Scrum Master
- L. Thurm - Tester
- ~~J. Phoomkhonsan - Projektleiter~~
- ~~J. Wolf - Developer~~

### Probleme und Lösungen

- Rückstand durch Arbeitsteilung und Ausfall von Zuständigen.
    - Fokus auf Arbeit an Pflicht-Issues.
- Merge Konflikte konnten wegen des Zeitmangels vor der Demo-Vorführung nicht behoben werden.
    - Die Merge-Konflikte wurden kurze Zeit nach der Demo-Vorführung behoben und hochgeladen.


### Reflektion über die Woche

Aufgrund des Ausfalls der Gruppenmitglieder lagen wir in dieser Woche bereits zurück und mussten unseren Workload dementsprechend anpassen, um die Applikation, im gegebenem Zeitrahmen, funktionsfähig zu bekommen.


### Zusammenfassung

In dieser Woche wurden die Web-Templates und die Controller fertig programmiert. Die Applikation war zum Ende dieser Woche demonstrations-bereit.


[AWT]: https://en.wikipedia.org/wiki/Abstract_Window_Toolkit
[Spark]: http://sparkjava.com/
[mustache]: http://mustache.github.io/
[MVC]: https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller
[cfg4j]: http://www.cfg4j.org/
[Selenium]: https://en.wikipedia.org/wiki/Selenium
[CRUD]: https://en.wikipedia.org/wiki/Create,_read,_update_and_delete
[SQLite]: https://www.sqlite.org/index.html
[Semantic]: https://semantic-ui.com/
