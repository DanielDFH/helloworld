# Einleitung

Dieses Projekt wurde im Rahmen des Moduls “Softwaretechnik II” an der Fachhochschule Lübeck erarbeitet.
Das Kennenlernen und das Anwenden der agilen Softwareentwicklungsverfahren, des Schätzen eines Projekts und das Durchführen von Usability-Tests,
die Weiterentwicklung eines Programms, waren das Ziel des Projekts. “Digitale Geldbörse” ist die vervollständigte Applikation mit dem Web-User-Interface,
das durch die Aufgabenstellungen des Moduls und durch das Programmkonzept entstand.
