# Branching Modell

Der *Master*-Branch hat immer einen stabilen Zustand.
Potentielle Nutzer sollten den Master-Branch auschecken und davon ausgehen
können, dass dieser immer eine funktionierende Version der Software enthält.

Für jedes Feature wird ein *Feature*-Branch erstellt, welcher nach Abschluss in
den Master-Branch gemerged wird.  Die Namen von Feature-Branches haben
folgendes Format: `feature/das-ist-ein-beispiel`

Für jedes Release wird ein Tag vom Master-Branch angelegt.
Release-Tags haben folgendes Format: `v1.2.3`, wobei die Version als Major,
Minor und Patch angegeben wird.  (Wobei natürlich fraglich ist, ob dieses
Versionsschema im Rahmen des Praktikums überhaupt vollständig eingesetzt werden
kann.)
