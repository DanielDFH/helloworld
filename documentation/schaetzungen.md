# Schätzungen

## Story-Points und Velocity
Die Gruppe hat sich darauf festgelegt, dass die Velocity 2 Stunden pro Story-Point entspricht.

Folgende Tabelle enthält die von unseren Gruppenmitgliedern geschätzen Story-Points für alle initialen Issues:

| Issue # 	|                    Titel                   	|      Am Anfang      	| Am Ende   | Pflicht 	|
|:-------:	|:------------------------------------------:	|:-------------------:	|:-------:	|:-------:	|
|    1    	|   Bezeichnungen nach Englisch übersetzen   	|   1, 1, 1, 1, 1, 1  	|    1    	|         	|
|    2    	|          Web-Framework integrieren         	|  3, 5, 5, 5, 8, 13  	|    8    	|      X   	|
|    3    	|         Autor-Kommentare entfernen        	|   1, 1, 1, 1, 1, 1  	|    1    	|         	|
|    4    	|    Konfigurations-System implementieren    	|   1, 1, 1, 3, 3, 5  	|    1    	|      X   	|
|    5    	| Integrationstests mit Selenium vorbereiten 	|  3, 5, 5, 8, 8, 13  	|    13   	|      X   	|
|    6    	|             Nutzer registrieren            	|   3, 3, 5, 5, 5, 8  	|    5    	|      X   	|
|    7    	|                 User Login                 	|   1, 1, 1, 2, 3, 5  	|    5    	|      X   	|
|    8    	|                Nutzer Logout               	|   1, 1, 2, 3, 3, 5  	|    3    	|      X   	|
|    9    	|             Ausleihen/Verleihen            	|   3, 3, 3, 5, 5, 8  	|    5    	|       X  	|
|    10   	|         Verlauf grafisch darstellen        	| 5, 8, 8, 13, 13, 21 	|    13   	|         	|
|    11   	|             Statistik anzeigen             	| 8, 8, 8, 13, 13, 13 	|    13   	|         	|
|    12   	|          Objekte anlegen / löschen         	|   3, 3, 3, 5, 5, 8  	|    5    	|       X  	|


Während des Sprints, sind wir auf weitere Probleme gestoßen für die wir ein Issue angelegt haben. Diese Issues haben wir allerdings nicht nochmal geschätzt.
Diese Issues wurden angelegt um den anderen Gruppenmitgliedern zu zeigen, dass an der Stelle noch Arbeit ist.


## Burn-Down-Chart

![](burndown.png)


## Reflektion über Abweichungen
Wir haben die Proportionen bezogen auf die Story-Points für die einzelnen Issues gut gewählt, allerdings war die Velocity zu groß, so war die geringste  Dauer die man durch die Story-Points abdecken konnte 2 Stunden.
Wir haben simple Issues mit einem Story-Point belegt, obwohl uns zu Beginn bereits bewusst war, dass die Issues vermutlich kürzer als 2 Stunden dauern.

Wir haben die Story-Points gut verteilt, allerdings waren diese zu Grob um die Dauer der Issues genau einzuteilen bzw. zu bestimmen.
