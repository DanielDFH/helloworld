## Sprint (28.5 - 3.6)

Anstatt des ursprünglichen Desktop UIs ([AWT][]) sollte ein Web-Server
eingesetzt werden.  Dazu wurde das [Spark Web-Framework][Spark], sowie die
[Mustache Template-Engine][mustache] in die Applikation integriert und
eine Beispiel-Seite inklusive Integrartions-Test implementiert.
Dies sollte als Referenz für die spätere Implementation von konkreten Seiten
nach dem [MVC-Pattern][MVC] dienen.

Damit die Applikation ohne Rekompilierung an verschiedene Umgebungen angepasst
werden kann, wurde die Konfiguration mittels der [cfg4j Bibliothek][cfg4j] in
eine Datei ausgelagert.

Zudem wurde die Struktur der Dokumentation angelegt, sowie eine interne
Dokumentation erstellt, welche die genutzten Web-Technologien kurz erläuterte.


## Sprint (4. - 10.6)

Kleinere Korrekturen am Build-System sowie der Versionverwaltung.

Die Dokumentation wurde etwas verbessert.


## Sprint (11. - 17.6)

Die für Integrationstests aufgesetzte Testumgebung wurde verbessert:

- Der Web-Server verwendet einen zufälligen verfügbaren Port, womit parallele
  Tests ermöglicht werden.
- Es wird jeweils eine frische In-Memory Datenbank aufgesetzt.

Es wurden Desktop spezifische, nun ungenutzte, Klassen aus dem Quellcode
entfernt.  Zudem wurden viele Variablen- und Methodennamen ins Englische
übersetzt.

Um bestimmte Entitäten, wie Beispielsweise Währungen, in der Web-Oberfläche
komfortabel verwalten zu können, wurden entsprechende Seiten angelegt.
Diese ermöglichen das Erstellen, Auslesen, Ändern und Löschen von Enitäten.
([CRUD][])

Für eine etwas bessere Nutzererfahrung im Falle von Fehlern, wurden Web-Seiten
angelegt welche dem Nutzer in diesem Falle angezeigt werden.


[AWT]: https://en.wikipedia.org/wiki/Abstract_Window_Toolkit
[Spark]: http://sparkjava.com/
[mustache]: http://mustache.github.io/
[MVC]: https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller
[cfg4j]: http://www.cfg4j.org/
[Selenium]: https://en.wikipedia.org/wiki/Selenium
[CRUD]: https://en.wikipedia.org/wiki/Create,_read,_update_and_delete
