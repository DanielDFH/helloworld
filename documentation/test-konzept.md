# Test Konzept

## Konzept

Wir haben uns entschieden als Test-Driver [JUnit] zu verwenden, da dies eine für Java gängige Bibliothekt ist.
JUnit wird von Maven gestartet und orchestriert die Ausführung aller Tests.

Für Integrationstests wird eine Testumgebung aufgesetzt:
Diese nutzt die Fähigkeit von SQLite aus [Datenbanken im RAM][SQLite In-Memory DB] zu erzeugen.
Zudem wird das [Selenium-Framework][Selenium] verwendet um einen ferngesteuerten Browser zu erzeugen.
Dieser kann in Java über sogenannte [WebDriver][Wikipedia WebDriver] angesprochen werden.


## Probleme

Während der Implementation der Tests sowie der Einbindung der entsprechenden Software sind keine Probleme aufgetreten.

Insbesondere die Integrationstests waren aufgrund der verwendeten Technologien (virtueller Browser, In-Memory Datenbank) einfach umzusetzen.


[JUnit]: https://junit.org/junit5
[Selenium]: https://en.wikipedia.org/wiki/Selenium
[SQLite In-Memory DB]: https://sqlite.org/inmemorydb.html
[Wikipedia WebDriver]: https://en.wikipedia.org/wiki/Selenium_(software)#Selenium_WebDriver
