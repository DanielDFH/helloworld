# Fazit

In diesem Projekt wurde nach agilem Softwareentwicklungsvervahren "SCRUM" die Weiterentwicklung des ausgewählten Programms "Digitale Geldbörse" realisiert.
Das entstandene Programm hat sich im Usability-Test gut gezeigt und wurde im Durchschnitt sehr gut bewertet.
Das Erlernen der neuen, gewählten Technologien, wie Semantic-UI, mustache und Spark, erfolgte nebenbei mit dieser Weiterentwicklung.
Das Arbeiten nach SCRUM-Verfahren hat mit sich viele Vorteile gebracht.
Auf der einen Seite wurde es bei der Umsetzung festgestellt, dass das Gefühl bei den Issues-Schätzungen oder den Dead-Line-Terminen nicht der Praxis entsprach. Das erklärt sich aber damit, dass es zu wenig Praxiserfahrungen in dem agilen Softwareentwicklungsverfahren gab und beim nächsten Mal genauere Schätzungen und Dead-Line-Termine abgegeben werden könnten.
Auf der anderen Seite brach die Auswertung der Usability-Tests neue Verbesserungsvorschläge mit sich.

Wegen der Praxiserfahrungen und der Zwangsminimierung der Gruppe wurden die Dead-Lines unter Zeitmangel erfüllt. Leider sind diese nicht immer zum gesetzten Dead-Line abgegeben worden. Nichtsdestotrotz wurden die Anforderungen erfüllt und nachgereicht.

Die Bearbeitung der Aufgabenstellungen hat uns, Mitglieder der Gruppe 3Q, zum Ziel des Projekts geführt.
