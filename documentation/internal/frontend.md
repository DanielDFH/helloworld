# Frontend mit Spark, Mustache und HTML

## Technologie Überblick

In Java kommt Spark als Web-Framework zum Einsatz.
Damit existiert ein einfacher Webserver, sowie eine API mit der u.A. auf
HTTP-Anfragen reagiert werden kann Endpunkt.

[Spark Dokumentation][]

Jede unterstützte Kombination von HTTP-Methode und URL-Pfad wird Route oder
Endpunkt genannt.  Z.B. `GET /index.html` oder `PUT /user/123`
Weitere Informationen zu HTTP-Methoden finden sich beispielsweise auf
[mozilla.org][MDN HTTP Methods] und [Wikipedia][Wikipedia REST].

Da viele Endpunkte HTML-Seiten liefern und es unpraktisch ist diese direkt in
Java zu definieren, wird [Mustache][]
als Template-Engine verwendet.  Im folgenden Beispiel werden zwei Variablen,
`title` und `url`, im Template verarbeitet:

Im Callback des Endpunktes:
```java
Map<String, Object> model = new HashMap<>();
model.put("title", "Example");
model.put("url", "http://example.org");
return ViewUtil.render(request, model, "example.mustache");
```

Template (`example.mustache`):
```html
<a href="{{url}}">{{title}}</a>
```

Als CSS Framework kommt [Semantic UI][] zum Einsatz.

[Dokumentation][Semantic UI Dokumentation]


## Tests

Integrationstests nutzen das Selenium-Framework um einen ferngesteuerten
Browser zu erzeugen.  Dieser kann in Java über sogenannte
[WebDriver][Wikipedia WebDriver] angesprochen werden.

[Beispiel und Dokumentation][WebDriver Beispiel]

Wenn wir Integrationstests schreiben ist folgendes zu beachten:

- Die Test-Klasse muss mit `@Tag("integration")` annotiert werden.
  So können Unit- von Integrationstests unterschieden werden.
- Die `TestEnvironment`-Instanz erzeugt die Applikation mit einer für den
  Integrationstest geeigneten Umgebung.  D.h. bspw. eine In-Memory-Datenbank
  sowie der WebDriver.

```java
@Tag("integration")
public class ExampleControllerTest {
    TestEnvironment env;

    @BeforeEach
    public void beforeEach() {
        env = new TestEnvironment();
    }

    @AfterEach
    public void afterEach() {
        env.close();
    }

    @Test
    public void test() {
        env.webDriver.get(env.getBaseUrl()+ Path.Web.EXAMPLE);
        assertEquals("Example", env.webDriver.getTitle());

        List<String> personNames = new ArrayList<>();
        for(WebElement element : env.webDriver.findElements(By.cssSelector(".card .content .header"))) {
            personNames.add(element.getText());
        }
        assertTrue(personNames.contains("Quack"));
        assertTrue(personNames.contains("Igel"));
    }
}
```


[MDN HTTP Methods]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
[Wikipedia REST]: https://en.wikipedia.org/wiki/Representational_state_transfer#Relationship_between_URL_and_HTTP_methods
[Mustache]: http://mustache.github.io/mustache.5.html
[Semantic UI]: https://semantic-ui.com
[Semantic UI Dokumentation]: https://semantic-ui.com/introduction/getting-started.html
[Spark Dokumentation]: http://sparkjava.com/documentation
[Wikipedia WebDriver]: https://en.wikipedia.org/wiki/Selenium_(software)#Selenium_WebDriver
[WebDriver Beispiel]: https://docs.seleniumhq.org/docs/03_webdriver.jsp#introducing-the-selenium-webdriver-api-by-example
